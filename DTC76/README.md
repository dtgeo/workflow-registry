## DTC-E6

* Last updated: 2024-10-09
* Updated by: S. Ceylan

This DTC is being re-designed to contain only a single STEP where all components originally listed as below will be distributed as a docker container.
All steps except ST760106 and ST760202 have been completely or partially developed as individual tools. We currently work on integration.

|   Step   | Name                                                                         | Status | 
| :------: | ---------------------------------------------------------------------------- | :----: | 
| ST760101 | Data Assimilation #1: real-time seismic waveform streams                     | Completed; not integrated      | 
| ST760102 | Data Assimilation #2: earthquake alerts                                      | Integrated       | 
| ST760103 | Data Assimilation #3: rapid raw strong motion parameters (RRSM)              | Integrated       | 
| ST760104 | Data Assimilation #4: macroseismic intensity reports                         | Integrated       | 
| ST760105 | Data Assimilation #5: engineering strong motion parameters (ESM)             | Integrated       | 
| ST760106 | Data Assimilation #6: dynamic ground-motion (GM) models/site amplification   | Future dev.; not completed; not integrated       | 
| ST760201 | Modeling #1: FinDer (Finite-fault Rupture Detector) -AI                      | Integrated       | 
| ST760202 | Modeling #2: ML-based algorithm to predict GM intensities in quasi real time | Future dev.; not integrated       | 
| ST760203 | Modeling #3: ShakeMaps                                                       | Completed; Not integrated       | 
| ST760204 | Modeling #4: SeisComP                                                        | Completed; Not integrated       | 
| ST760301 | Products: evolutionary data-driven ShakeMaps                                 | Completed; Not integrated       | 
|          | |(*Integrated: modules are integrated in the repo in ```src```*) |

