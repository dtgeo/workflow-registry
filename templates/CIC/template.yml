---
spec:
  inputs:
    prestage:
      default: CIC_PRE
    stage:
      default: CIC
    depends_on:
      type: array
      default: []
    cic_builder_job_script:
      type: string
      default: "./bin/cic_builder.sh"
---
variables:
  BUILD_REPO_BASE: ${CI_REGISTRY}/${CI_PROJECT_PATH}
  BUILD_REPO_NAME: container_registry/cic_builder
  BUILD_REPO_TAG: latest
  BUILD_PACKAGE_NAME: eflows4hpc_image_creation
  BUILD_DEPLOY_SINGULARITY: 'no'

.create-test-container: &create-test-container
  stage: $[[ inputs.stage ]]
  image: ${BUILD_REPO_BASE}/${BUILD_REPO_NAME}:${BUILD_REPO_TAG}
  services:
    - name: docker:dind-rootless
      alias: docker
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    DTC_PLATFORM_: ${DTC_PLATFORM}
    DTC_ARCHITECTURE_: ${DTC_ARCHITECTURE}
    DTC_CONTAINER_ENGINE_: ${DTC_CONTAINER_ENGINE}
    DTC_ID_: ${DTC_ID}
    DTC_WORKFLOW_ID_: ${DTC_WORKFLOW_ID}
    DTC_STEP_ID_: ${DTC_STEP_ID}
    DTC_VERSION_: ${DTC_VERSION}
    DTC_REPO_NAME_: ${DTC_REPO_NAME}
    DTC_GIT_FORCE_FLAG_: ${DTC_GIT_FORCE_FLAG}
    DTC_GIT_PUSH_: ${DTC_GIT_PUSH}
    DTC_WORKSPACE_PATH_SUFFIX_: ${DTC_WORKSPACE_PATH_SUFFIX}
    DTC_CIC_BUILDER_JOB_SCRIPT: $[[ inputs.cic_builder_job_script ]]
    CIC_BUILDER_WORKFLOW_REPOSITORY: ${CI_SERVER_URL}/${CI_PROJECT_PATH}
    CIC_BUILDER_URL: ${CI_REGISTRY}
    CIC_BUILDER_REGISTRY_USER: ${CONTAINER_REGISTRY_DEPLOY_TOKEN_ID}
    CIC_BUILDER_REGISTRY_PASSWORD: ${CONTAINER_REGISTRY_DEPLOY_TOKEN}
    CIC_BUILDER_IMAGES_PREFIX: ${CI_REGISTRY_IMAGE}/container_registry
    CIC_WORKFLOW_REPOSITORY_BRANCH: ${CI_COMMIT_REF_NAME}
    CIC_BUILDER_TMP_FOLDER: /tmp
    CIC_BUILDER_HOME: /image_creation
    CIC_BUILDER_BASE_IMAGE: 'ghcr.io/eflows4hpc/spack_base:0.20.1'
    CIC_BUILDER_DOCKERFILE: Dockerfile.spack
    CIC_BUILDER_SPACK_CFG: /software-catalog/cfg
    CIC_BUILDER_MAX_CONCURRENT_BUILDS: 1
    CIC_BUILDER_SINGULARITY_SUDO: ${BUILD_DEPLOY_SINGULARITY}
    CIC_BUILDER_DEBUG: 'yes'
    DTC_DEBUG_: ${DTC_DEBUG}
  dependencies:
    - $[[ inputs.depends_on ]]

create-dtc-test-container:
  <<: *create-test-container
  when: on_success
  script:
    - |
      echo "DEBUG: DTC_STEP_PATH = ${DTC_STEP_PATH}"
      echo "DEBUG: DTC_EFLOWS4HPC_YAML = ${DTC_EFLOWS4HPC_YAML}"
      if [ -z "${DTC_STEP_PATH}" ] || [ ! -f "${DTC_EFLOWS4HPC_YAML}" ]; then
        echo "Warning: missing eflows4hpc.yaml file for DTC. Skipping container creation."
        exit 0
      fi
      if [[ "x${DTC_WORKFLOW_ID_}" != "x" && "x${DTC_STEP_ID_}" != "x" ]]; then
        source "${DTC_CIC_BUILDER_JOB_SCRIPT}"
        w="${DTC_WORKFLOW_ID_}"
        for s in ${DTC_STEP_ID_}; do
          echo "Evaluating step $s"
          if [[ "${s#ST}" =~ "${DTC_WORKFLOW_ID_#WF}" ]]; then
            echo "calling cic_builder function"
            cic_builder
            echo "finished cic_builder function"
          else
            echo "Error: step $s id syntax don't include the right workflow ${DTC_WORKFLOW_ID_} id. Example: ST<WPnn><WFnn><STnn>"
            exit 1
          fi
        done
      else
        echo "Warning: no changes applied to Workflows"
      fi

create-all-test-containers:
  <<: *create-test-container
  when: manual
  script:
    - |
      source "${DTC_CIC_BUILDER_JOB_SCRIPT}"
      for w in $(ls -d WF*); do
        DTC_WORKFLOW_ID_="${w}"
        for s in $(cd ${w}; ls -d ST*; cd ..); do
          if [ -f "${w}/${s}/eflows4hpc.yaml" ]; then
            DTC_STEP_ID_="${s}"
            cic_builder
          else
            echo "Warning: missing eflows4hpc.yaml file for step ${s}"
          fi
        done
      done

create-single-test-containers:
  <<: *create-test-container
  when: manual
  rules:
    - if: '$DTC_STEP_ID == null'
  script:
    - |
      source "${DTC_CIC_BUILDER_JOB_SCRIPT}"
      DTC_NO=${DTC_STEP_ID:2:2}
      DTC_ID=DTC${DTC_NO}
      WORKFLOW_NO=${DTC_STEP_ID:4:2}
      DTC_WORKFLOW_ID=WF${DTC_NO}${WORKFLOW_NO}
      STEP_FOLDER_PATH="${DTC_ID}/${DTC_WORKFLOW_ID}/${DTC_STEP_ID}"
      if [ -f "${STEP_FOLDER_PATH}/eflows4hpc.yaml" ]; then
        # Variables required to fill in dtgeo_request.json.tpl and bin/cic_builder.sh
        DTC_ID_=${DTC_ID}
        DTC_WORKFLOW_ID_=${DTC_WORKFLOW_ID}
        w=${DTC_WORKFLOW_ID}
        s=${DTC_STEP_ID}
        cic_builder
      else
        echo "Warning: missing eflows4hpc.yaml file for step ${DTC_STEP_ID}"
        exit 1
      fi
