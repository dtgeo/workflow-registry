# WP7 (Earthquakes) implementation status

|  DTC   | Name                                             | Number of WFs. | Number of Steps |
| :----: | ------------------------------------------------ | :------------: | :-------------: |
| DTC-E1 | Probabilistic Seismic Hazard and Risk Assessment |       4        |       16        |
| DTC-E2 | Earthquake short-term forecasting                |       2        |       17        |
| DTC-E3 | Tomography and Ground Motion Models (GMM)        |       1        |       10        |
| DTC-E4 | Fault rupture forecasting                        |       3        |       13        |
| DTC-E5 | Tomography and shaking simulation                |       2        |       16        |
| DTC-E6 | Rapid event and shaking characterization         |       3        |       11        |

The current implementation status of the DTCs is indicated by the following symbols:

|  Symbol   |      Status       |
| :-------: | :---------------: |
| &#x2705;  | Fully implemented |
| &#x1F51C; | Under development |
| &#x274C;  |  Not implemented  |


## DTC-E1

* Last updated: 20/02/2025
* Updated by: Nicolas Schmid

|   Step   | Name                                                                       |  Status   |                                                       Registry                                                        |
| :------: | -------------------------------------------------------------------------- | :-------: | :-------------------------------------------------------------------------------------------------------------------: |
| ST710101 | Integration of seismic catalogs from various APIs (EMEC, EPICA, and ESFM). | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710101) |
| ST710102 | Data Cleaning and feature selection                                        | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710101) |
| ST710103 | Statistical Analysis of the Earthquake Catalog                             | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710101) |
| ST710104 | Harmonize Active Fault Dataset                                             | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710101) |
| ST710105 | Update crustal faults model and subduction zones                           | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710101) |
| ST710201 | Seismogenic Sources Model (SSM)                                            | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710201) |
| ST710202 | Data retrieval and transformation                                          | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710201) |
| ST710203 | Estimation of Activity Rates                                               | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710201) |
| ST710204 | Long Term Earthquake Forecasts                                             | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710201) |
| ST710205 | Update of Seismogenic Source Model                                         | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7101/ST710201) |
| ST710301 | Use Source Model and Logic Tree XML files                                  | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7103/ST710301) |
| ST710302 | Hazard Calculation                                                         | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7103/ST710301) |
| ST710303 | Hazard Output                                                              | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7103/ST710301) |
| ST710401 | Use input files for risk calculation                                       | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7104/ST710401) |
| ST710402 | Risk Calculation                                                           | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7104/ST710401) |
| ST710403 | Risk Output                                                                | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC71/WF7104/ST710401) |

## DTC-E2

* Last updated: 19/02/2025
* Updated by: Nicolas Schmid

|   Step   | Name                                    |  Status   |                                                        Registry                                                        |
| :------: | --------------------------------------- | :-------: | :--------------------------------------------------------------------------------------------------------------------: |
| ST720101 | Waveforms(EPOS)                         | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7201/QuakeFlow) |
| ST720102 | Picking                                 | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7201/QuakeFlow) |
| ST720103 | Association                             | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7201/QuakeFlow) |
| ST720104 | Locations, magnitudes, focal mechanisms | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7201/QuakeFlow) |
| ST720105 | High-res catalog                        | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7201/QuakeFlow) |
| ST720201 | Seismicity catalog                      | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7201/QuakeFlow) |
| ST720202 | RAMSIS                                  | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720202)  |
| ST720203 | Activity monitoring/scheduling          | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720202)  |
| ST720204 | CRS forecast                            | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720204)  |
| ST720205 | ETAS forecast                           | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720204)  |
| ST720206 | ETAS parameter inversion                | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720204)  |
| ST720207 | Sequence specific model update          | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720204)  |
| ST720208 | Catalog simulation                      | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720204)  |
| ST720209 | Database                                | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720202)  |
| ST720210 | Web service                             | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720202)  |
| ST720211 | Forecast visualization                  | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC72/WF7202/ST720211)  |
| ST720212 | Testing                                 | &#x274C;  |                                                                                                                        |

## DTC-E3

* Last updated: 19/10/2024
* Updated by: Johannes Kemper

|   Step   | Name                                               |  Status   |                                                       Registry                                                        |
| :------: | -------------------------------------------------- | :-------: | :-------------------------------------------------------------------------------------------------------------------: |
| ST730101 | Observed Ground Motion Data                        | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC73/WF7301/ST730101) |
| ST730102 | Anomaly Detection                                  | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC73/WF7301/ST730102) |
| ST730103 | Simulation Data                                    |           |                                                                                                                       |
| ST730104 | Simulated Ground Motion, Comparison and Validation | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC73/WF7301/ST730104) |
| ST730105 | Data Assimilation and Harmonisation                | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC73/WF7301/ST730105) |
| ST730106 | Near-fault Model                                   |           |                                                                                                                       |
| ST730107 | Ground Motion Model                                | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC73/WF7301/ST730107) |
| ST730108 | GMM Coefficient Set                                |           |                                                                                                                       |
| ST730109 | Dynamically Updated GMM Coefficients               | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC73/WF7301/ST730109) |
| ST730110 | Updated Hazard & Risk Products                     |           |                                                                                                                       |


## DTC-E4

* Last updated: 16/10/2024
* Updated by: Johannes Kemper

|   Step   | Name                                                                             |  Status   |                                                       Registry                                                        |
| :------: | -------------------------------------------------------------------------------- | :-------: | :-------------------------------------------------------------------------------------------------------------------: |
| ST740101 | Dynamic inversion data                                                           |           |                                                                                                                       |
| ST740102 | Dynamic source inversion                                                         |           |                                                                                                                       |
| ST740103 | Data-driven dynamic models                                                       |           |                                                                                                                       |
| ST740201 | Kinematic inversion data                                                         |           |                                                                                                                       |
| ST740202 | Kinematic source inversion (output)                                              |           |                                                                                                                       |
| ST740203 | Kinematic model                                                                  |           |                                                                                                                       |
| ST740204 | Kinematically-informed dynamic rupture simulations                               | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC74/WF7402/ST740204) |
| ST740205 | Kinematically-informed dynamic rupture mode                                      |           |                                                                                                                       |
| ST740301 | Multi-physics constraints (input files for the creation of the scenario catalog) |           |                                                                                                                       |
| ST740302 | Forward dynamic rupture simulation                                               | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC74/WF7403/ST740302) |
| ST740303 | Ensemble scenarios                                                               |           |                                                                                                                       |
| ST740304 | Seismic and GPS data                                                             |           |                                                                                                                       |
| ST740305 | Single scenario dynamics and shake maps                                          | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC74/WF7403/ST740305) |

## DTC-E5

* Last updated: 25/11/2024
* Updated by: Johannes Kemper

|   Step   | Name                                         |  Status   |                                                       Registry                                                        |
| :------: | -------------------------------------------- | :-------: | :-------------------------------------------------------------------------------------------------------------------: |
| ST750101 | Data catalog Update                          |           |                                                                                                                       |
| ST750102 | Local / Regional / Global Updates Definition |           |                                                                                                                       |
| ST750103 | Model Extraction                             |           |                                                                                                                       |
| ST750104 | Inversion Iterations                         | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC75/WF7501/ST750104) |
| ST750105 | Model Update                                 |           |                                                                                                                       |
| ST750106 | User Model Validation                        |           |                                                                                                                       |
| ST750201 | Assimilate Data                              | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC75/WF7502/ST750201) |
| ST750202 | Acquire Source Parameters                    |           |                                                                                                                       |
| ST750203 | Build Input Parameters                       | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC75/WF7502/ST750203) |
| ST750204 | Earthquake HPC Simulations                   |           |                                                                                                                       |
| ST750205 | Evaluate Event Updates                       |           |                                                                                                                       |
| ST750206 | Enable Urgent Computing                      |           |                                                                                                                       |
| ST750207 | MLES Map                                     |           |                                                                                                                       |
| ST750208 | Post-process Results                         |           |                                                                                                                       |
| ST750209 | Gather Outputs                               |           |                                                                                                                       |
| ST750210 | Shake Maps Library Update                    |           |                                                                                                                       |

## DTC-E6

* Last updated: 16/10/2024
* Updated by: Johannes Kemper

|   Step   | Name                                                                         |  Status   |                                                       Registry                                                        |
| :------: | ---------------------------------------------------------------------------- | :-------: | :-------------------------------------------------------------------------------------------------------------------: |
| ST760101 | Data Assimilation #1: real-time seismic waveform streams                     |           |                                                                                                                       |
| ST760102 | Data Assimilation #2: earthquake alerts                                      | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC76/WF7601/ST760102) |
| ST760103 | Data Assimilation #3: rapid raw strong motion parameters (RRSM)              | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC76/WF7601/ST760102) |
| ST760104 | Data Assimilation #4: macroseismic intensity reports                         | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC76/WF7601/ST760102) |
| ST760105 | Data Assimilation #5: engineering strong motion parameters (ESM)             | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC76/WF7601/ST760102) |
| ST760106 | Data Assimilation #6: dynamic ground-motion (GM) models/site amplification   |           |                                                                                                                       |
| ST760201 | Modeling #1: FinDer (Finite-fault Rupture Detector) -AI                      |           |                                                                                                                       |
| ST760202 | Modeling #2: ML-based algorithm to predict GM intensities in quasi real time |           |                                                                                                                       |
| ST760203 | Modeling #3: ShakeMaps                                                       |           |                                                                                                                       |
| ST760204 | Modeling #4: SeisComP                                                        |           |                                                                                                                       |
| ST760301 | Products: evolutionary data-driven ShakeMaps                                 |           |                                                                                                                       |