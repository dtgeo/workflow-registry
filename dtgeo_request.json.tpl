{

    "machine": {

         "platform": "${DTC_PLATFORM_}", "architecture": "${DTC_ARCHITECTURE_}", "container_engine": "${DTC_CONTAINER_ENGINE_}"

         },

    "workflow": "${DTC_ID_}/${DTC_WORKFLOW_ID_}",

    "step_id" : "${DTC_STEP_ID_}",

    "force": "${DTC_GIT_FORCE_FLAG_}",

    "push": "${DTC_GIT_PUSH_}",

    "debug": "${DTC_DEBUG_}",

    "path": "./"

}
