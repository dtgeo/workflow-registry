# The DT-GEO Workflow Registry

This repository stores the Workflow descriptions using the Spack environment methodology. This description consists of the code of the different steps and the required software per step.

[[_TOC_]]

## Repository structure

Workflow descriptions have to be included inside this repository according to the following structure.

```
workflow-registry
  |- DTC51                          DTC name format: DTC<WPn><DTCn>
  |    |- WF5101                    Workflow name format: WF<WPn><DTCn><WFnn>
  |    |    |- ST510101             Step name format: ST<WPn><DTCn><WFnn><STnn>
  |    |    |   |- eflows4hpc.yaml  Software requirements for this workflow step as a Spack environment specification (same as spack.yaml)
  |    |    |   |- src              PyCOMPSs code of the workflow step
  |    |           ...
  |    |- WF5102
  |    |    |- ST510201
  |            ...
  |- DTC52
  |	 ...

```

<!--
## Including new Workflows
To include new workflows in the repository, first create a new fork of the repository and include a new folder for the different workflow steps. Finally, create a pull request with the new workflow description. This pull request will be reviewed and included in the repository.
-->

## Contributing to the Workflow Registry

Workflows operated by DTCs are expected to be composed of steps. Accordingly, the Workflow Registry uses a hierarchical folder structure (DTC >> Workflow >> Step), where Steps appear at the lower level. This structure is leveraged by the [Container Image Creation service](https://github.com/eflows4hpc/image_creation) that creates the Docker images for each of those steps.

In DT-GEO, such Git repositories might be located in external platforms, and might be public or private. In order to cope with the existing heterogeneity and facilitate the task to register any new release of a workflow step, the procedure described here **does not imply pushing any code to the Workflow Registry**, but instead the registry will pull the new releases upon DTC developer's request.

To this end, [Git submodules](https://git-scm.com/docs/git-submodule) are used. The **DTC developers are responsible to add new steps, and update any release thereof, in the Workflow Registry**. The frequency and size of such releases is subject to decision of the DTC development team.

In the following sections you will find the procedure to contribute with new releases of your DTC. Before that it is important to remark that:
- The code for **each Step** (within a Workflow, within a DTC) **_shall be maintained either i) in an individual Git repository or ii) using separate branches within the same Git repository_** and registered as a Git submodule in the registry.
- The **version of the workflow step can only be tracked with branches** (Git submodule limitation).
- **Private repositories will require the issuance of an access token** as it is [described in the FAQ](#enabling-access-to-private-workflow-repositories). This will be need to enable the CI/CD pipeline work.

In short, the procedure to register a new release of a step in a workflow is as follows:

#### 1) Get the latest version of the Workflow Registry repository

In order to add or update an existing Git submodule, you shall fetch the latest version of the `main` branch. There are two common scenarios:

##### 1.1) Cloning the repository

This shall be done when you don't have a local copy of the Workflow Registry repository:

```bash
$ git clone https://gitlab.com/dtgeo/workflow-registry
```
<ins>Note</ins>: do not use `--recurse-submodules` option as it will ask for authentication for all the not publicly accessible submodules.

##### 1.2) Pulling changes

When you already have a copy of the Workflow Registry repository in your local workstation, be sure to **pull the latest changes of the `main` branch** before proceeding with the next steps:

```bash
$ git pull origin main
```

#### 2) Add the required changes

After cloning the `main` branch, use a new branch where the required changes will be added. This branch will be eventually used as the source branch for the merge request as part of the last step.

Creating a branch from the latest changes in `main` could be done in various ways, here we propose the following command:

```bash
$ git checkout -b <feature/branch>
```

where:
- `<feature/branch>` is the name of the branch that will include the required changes. It is recommend to use a meaningful name.

Now follow either section [2.1) the first time you are adding the workflow step](#21-add-a-new-step-in-the-workflow), or [2.2) if you would like the Workflow Registry to track a new release of the workflow step](#22-new-release-of-a-workflow-step):

##### 2.1) Add a new step in the workflow

**_This action shall only be done if the step has not yet been added to the list of submodules_**.

The repository containing the code of the workflow step must be tracked as a Git submodule. So, every time a new step is required to be added to a given workflow, the repository must be added with:

```bash
$ git submodule add -b <branch> <url> <path>
```

where:
- `<branch>` is the target branch to fetch from the upstream repository.
- `<url>` is the URL to the upstream repository.
- `<path>` is the relative location of the step within the workflow.

This command adds a new definition of the step in the `.gitmodules` file.

As mentioned earlier, there are two main approaches to maintain the code for a step in a workflow:

1. Use an individual Git repostory, e.g.:

```bash
$ git submodule add -b 1.0.0 https://gitlab.com/myexampleorg/st510101 DTC51/WF5101/ST510101/src
```

2. Use separate branches within the same Git repository (which contains the workflow code), e.g.:

```bash
$ git submodule add -b st510101/1.0.0 https://gitlab.com/myexampleorg/wf5101 DTC51/WF5101/ST510101/src
```

<ins>Note</ins>:
- In the approach #2 you should follow some convention that allows to version every new release of a given step, since as it will be explained in the following section, Git submodules only allows to use branches, not tags nor commit IDs. In the example above the convention used is to name the branch as `<dtgeo-step-id>/<x.y.z>`.
- If the repository added as a Git submodule is private, the `git submodule add` command will prompt for credentials.

In both approches, the resultant `.gitmodules` should now reflect the new combination of "repository URL + branch name" being added as a Git submodule (showing here the output of approach #2):

```bash
$ cat .gitmodules
[submodule "DTC51/WF5101/ST510101/src"]
	path = DTC51/WF5101/ST510101/src
	url = https://gitlab.com/myexampleorg/wf5101
	branch = st510101/1.0.0
```

**COMMIT & PUSH CHANGES**: at this point you must commit and push the changes done, otherwise they won't be tracked in the Workflow Registry. E.g.:

```bash
git commit -a -m "Registering new step ST510101 for DTC51/WF5101 (branch: st510101/1.0.0)"
git push <remote> <feature/branch>
```

where:
- `<remote>` is the remote name. The name `origin` is the one defined by default, but you could ensure that it points to the right repository by running `git remote -v`.
- `<feature/branch>` is the branch created in [section 2)](#2-add-the-required-changes).

<ins>Note</ins>: Write permission is required for the push action to work. Please contact maintainers to add your GitLab account as a contributor.


##### 2.2) New release of a workflow step

**_This action shall only be done if the step has been added to the list of submodules_**. If not, [follow section 1.1)](#11-add-a-new-step-in-the-workflow).

New releases of workflow steps already added as Git submodules (following the indications on the previous section) are only managed through branches, thus not tag names and/or commit IDs are allowed (Git submodules limitation). Consequently, every new release of a step in a workflow must be tracked in a different branch. [Semantic versioning](https://semver.org) is the recommended approach for tracking new releases.

The (new release) branch name can be set by either:
1. Running `git submodule set-branch` (note that the `set-branch` subcommand might not be available in older versions of the git client):

```bash
$ git submodule set-branch --branch <branch> <path>
```

where:
- `<path>` is the relative location of the existing step.

2. Editing directly the `.gitmodules` file to update the `branch` parameter.


Following the previous example, where version "1.0.0" was initially registered for the path `DTC51/WF5101/ST510101/src`, if the DTC developer wants to update this version to e.g. "1.1.0":

```bash
$ git submodule set-branch --branch 1.1.0 DTC51/WF5101/ST510101/src
```

Note that, **_at this point, the new release has not been fetched_**, only changed in the Git submodule configuration (check `git status`). Please follow next section in order to pull the new content from the updated release branch.

<ins>Hint</ins>: use `git submodule status` to check the current version (SHA) of the submodules being tracked in the registry. At this point this command should report the commit SHA of the previous branch, not the one being updated.

###### 2.2.1) Pull the latest changes of the release branch

Now, so as to complete the branch update, you should run the `git submodule update` command:

```bash
$ git submodule update --remote <path>
```

where:
- `<path>` is the relative location of the module.

This command should fetch the latest changes of the new branch, and store them under the required path in the Workflow Registry. In our example, running the command:

```bash
$ git submodule update --remote DTC51/WF5101/ST510101/src
```

should result in updating the contents of the branch `1.1.0` for the step ST510101.

<ins>Hint</ins>: running again `git submodule status` will acknowledge the new version.


**COMMIT & PUSH CHANGES**: at this point you must commit and push the changes done, otherwise they won't be tracked in the Workflow Registry. E.g.:

```bash
git commit -a -m "Updating existing step ST510101 for DTC51/WF5101: release 1.1.0"
git push <remote> <feature/branch>
```

where:
- `<remote>` is the remote name. The name `origin` is the one defined by default, but you could ensure that it points to the right repository by running `git remote -v`.
- `<feature/branch>` is the branch created in [section 2)](#2-add-the-required-changes).

<ins>Note</ins>: Write permission is required for the push action to work. Please contact maintainers to add your GitLab account as a contributor.

#### 3) Incorporate the changes to the main branch of the Workflow Registry

After successfully pushed your changes, [open a merge request (MR)](https://gitlab.com/dtgeo/workflow-registry/-/merge_requests) having `<feature/branch>` as the Source branch and `main` as the Target branch. This MR will be reviewed by maintainers.


<!--
    Enabling the CI pipeline
-->
## Enabling the CI pipeline

The workflow registry has been provisioned with a CI pipeline that executes the following stages:

1. Build and push Docker images for the workflow steps (through eFlows4HPC's [Container Image Creation (CIC)](https://github.com/eflows4hpc/image_creation) service)
2. QA assessemnt of the workflow code (through the [SQAaaS plaftorm](https://sqaaas.eosc-synergy.eu))

![Successful execution of the CI pipeline](figs/pipeline.png)

Some key features of the pipeline are:
- The pipeline reacts to merge-request (MR) events. The pipeline does not handle well changes in multiple steps and/or workflows added in the same MR, so it is important to stress that at present the **changes within a MR shall only be done to a single step**.
- The pipeline only proceeds with the execution of the aforementioned stages only if it founds a `eflows4hpc.yaml` file under the step folder. **If you want to enable the execution of the pipeline for a given step, please make sure that the `eflows4hpc.yaml` is present and it contains the required dependencies**. For instance, the following `eflows4hpc.yaml` installs `compss` and `fall3d` software dependencies.

```yaml
spack:
  specs:
  - compss
  - fall3d
  view: true
  concretizer:
    unify: true
```

### Build & push container images

Whenever the MR is opened, the GitLab pipeline leverages the CIC service to trigger the building and publishing of the Docker image for the modified step. This image will contain the step code (under `/ST<WPnn><WFnn><STnn>/src`) and the required software (Spack) dependencies installed. Remember that such dependencies are defined in the aforementioned `eflows4hpc.yaml` file.

As a result of the successful execution of the pipeline, the new Docker image for step will be available in the [DT-GEO workflow registry's container repo](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/container_registry). The (current) default policy is to avoid force-pushing, so if an image with the same identifier (name+tag) is already present in the container registry as a result of a previous execution, the newly-built image will not be uploaded. To circumvent this behaviour, the current approach is to remove the image before the execution of the pipeline, and thus, the creation of the MR.

### QA assessment

The quality assessment follows the creation of the Docker image. It uses the code repository defined for the step (Git submodule). By default, the code goes through the baseline criteria established by the SQAaaS platform (further [documentation about this service](https://docs.sqaaas.eosc-synergy.eu)). 

The results from the assessment are available as an artifact (JSON file) of the pipeline. For a human-readable version of the QA assessment results you should load this JSON file from the SQAaaS platform:
- Gather the URL of the JSON file by heading to the pipeline view and clicking on the `SQA` job:

![SQA assessment job](figs/pipeline1.png)

then use the `Browse` option within the Artifacts section to and in the next view, copy the URL of the SQAaaS report (`sqaaas.json` file):

![Artifacts section](figs/pipeline2.png)

- Now head to the [SQAaaS platform](https://sqaaas.eosc-synergy.eu), click on the `Quality Assessment & Awarding` module, then `Source code` tab. Once here, use the option `Load QAA report from a previous assessment`, and paste the (previously-gathered) URL of the `sqaaas.json` in the text box from option `By typing the URL of the JSON file`:

![SQAaaS platform](figs/pipeline3.png)

After this last step, and by clicking the `Start Source Code Assessment`, the SQAaaS report will be displayed.

The <ins>ultimate goal</ins> is to complement the QA assessment by adding the execution of the step and/or the entire workflow leveraging the Docker image being produced. This QA check shall be progressively integrated by the DTC developers (with the support of WP2+WP4) as the CWL+RO-Crate documents become available in the DT-GEO metadata catalog for each step and workflow. The [`cwltool`](https://cwltool.readthedocs.io/en/latest/index.html) command will be used to orchestrate the aforementioned CWL documents. 


<!--
    Frequently asked questions
-->
## FAQ

### Enabling access to private workflow repositories

***This action is just needed whenever the repository containing the workflow code has limited access. In most cases, this is only performed once (or anytime the access token needs to be renewed)***

#### 1) Create an access token for the target/private repository

**Within the target repository**, you need to create a token that will enable the CI/CD pipeline to read the repository:
- If using GitLab, follow the [steps to create a deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/#create-a-deploy-token)
- If using GitHub, follow the [steps to create an access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#creating-a-fine-grained-personal-access-token)

Save the values both for the username and the token, as both will be required in the next step.

**IMPORTANT REQUIREMENT**: the access token MUST have permission to read the repository (a clone operation will be performed by the CI/CD pipeline).

### 2) Store the token as a variable for CI/CD

Now, **within the [Workflow registry repository](https://gitlab.com/dtgeo/workflow-registry/)**, we need to keep track of the access token so the CI/CD pipeline is able to access the repository from the previous step. To this end, we will [create two project variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project) that MUST adhere to the following convention:

- User name of the token: `DTC<XY>_USERNAME`
- Token: `DTC<XY>_TOKEN`

where `<XY>` corresponds to the digits for "work package" (`X`) and the number of DTC within the work package (`Y`), i.e. following the same approach used in the DT-GEO metadata.
