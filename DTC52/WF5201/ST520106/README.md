# ST520106 - METEO-ERA5 repository

The gitmodule src@0214b7b8 is a link to the official repository of METEO-ERA5 within GEO3BCN official repository.

The `post_inst.sh` script is a bash script used to install all the necessary packages for METEO-ERA5 when an eflows4HPC image is created using this workflow registry repository.