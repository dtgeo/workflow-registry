#!/bin/bash

# Author: Eva Hernández Plaza ehernandez@geo3bcn.csic.es

# Update repositories and install the necessary dependencies
apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip \
    libnetcdff-dev \
    libopenjp2-7-dev \
    gfortran \
    make \
    unzip \
    git \
    cmake \
    wget \
    vim \
    nano \
    build-essential \
    libnetcdf-dev \ 
    libhdf5-dev \
    libfftw3-dev \
    nco \ 
    netcdf-bin

# Move example directory within METEO-ERA5 folder and rename src directory
mv /ST520106/src/examples /METEO-ERA5/
mv /ST520106/src /METEO-ERA5/meteo-era5

# Create the directory and download ECCODES
cd /METEO-ERA5/eccodes
wget https://confluence.ecmwf.int/download/attachments/45757960/eccodes-2.34.1-Source.tar.gz
tar -xzf eccodes-2.34.1-Source.tar.gz
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/src/eccodes ../eccodes-2.34.1-Source
make
ctest
make install

# Install Python libraries and update ECCODES definitions
pip3 install datetime \
   cfgrib \
   xarray \
   requests \
   urllib3 \
   eccodes \
   netcdf4 \
   dtcv2-util \
   cdsapi

# Run the ECCODES update script
cd /METEO-ERA5/meteo-era5/resources
./update_eccodes.sh ../../eccodes/eccodes-2.34.1-Source kwbc
./update_eccodes.sh /usr/src/eccodes/share/eccodes kwbc

# Set the environment variables
echo 'export ECCODES_DIR=/usr/src/eccodes' >> ~/.bashrc
echo 'export ECCODES_DEFINITION_PATH=/usr/src/eccodes/share/eccodes/definitions' >> ~/.bashrc
echo 'export ECMWFLIBS_ECCODES_DEFINITION_PATH=/usr/src/eccodes/share/eccodes/definitions/' >> ~/.bashrc
source ~/.bashrc
    
# Download and install CDO
wget https://code.mpimet.mpg.de/attachments/download/29019/cdo-2.3.0.tar.gz
tar -xzf cdo-2.3.0.tar.gz
cd cdo-2.3.0
./configure --with-netcdf=/usr/include
make
make install

# Set environment variables
echo 'export PATH="/home/eccodes/build/bin:/usr/src/eccodes/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
