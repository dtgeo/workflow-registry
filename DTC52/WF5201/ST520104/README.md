# ST520104 - METEO-GFS repository

The gitmodule src@ad08800a is a link to the official repository of METEO-GFS within GEO3BCN official repository.

The `post_inst.sh` script is a bash script used to install all the necessary packages for METEO-GFS when an eflows4HPC image is created using this workflow registry repository.