#!/bin/bash

#Author: Eva Hernández Plaza ehernandez@geo3bcn.csic.es

# Update repositories and install the necessary dependencies
apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip \
    libnetcdff-dev \
    libopenjp2-7-dev \
    build-essential \
    libaec-dev \
    zlib1g-dev \
    libcurl4-openssl-dev \
    libboost-dev \
    curl \
    zip \
    bzip2 \
    gfortran \
    make \
    unzip \
    git \
    cmake \
    wget \
    vim \
    nano \
    gcc \
    g++


# Install the Python libraries
pip3 install \
    datetime \
    xarray \
    requests \
    netcdf4 \
    dtcv2-util

# Move example directory within METEO-GFS folder and rename src directory
mv /ST520104/src/examples /METEO-GFS/
mv /ST520104/src /METEO-GFS/meteo-gfs

# Create and move to the working directory to download wgrib2
mkdir -p /METEO-GFS/wgrib2
cd /METEO-GFS/wgrib2

# Download wgrib2
wget -c ftp://ftp.cpc.ncep.noaa.gov/wd51we/wgrib2/wgrib2.tgz

# Extract the code
tar -xzvf wgrib2.tgz

# Move to the grib2 main directory to compile it
cd /METEO-GFS-TEST/wgrib2/grib2

# Export the necessary environment variables for compilation
echo 'export CC=gcc' >> ~/.bashrc
echo 'export FC=gfortran' >> ~/.bashrc
source ~/.bashrc

# Compile and creates the binary
make

# Remove any previous installation of grib2 and recreate the directory
rm -rf /usr/local/grib2/
mkdir -p /usr/local/grib2/

# Copy the wgrib2 binary to the /usr/local/bin directory
cp -rfv ./wgrib2/wgrib2 /usr/local/bin/wgrib2

# Clean temporary files
cd /METEO-GFS
rm -rf /METEO-GFS/wgrib2
