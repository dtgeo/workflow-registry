#!/usr/bin/env bash

function docker_login() {
  echo "DEBUG: test if dind service is up and running"
  echo "DEBUG: ${CIC_BUILDER_REGISTRY_PASSWORD} | docker login ${CI_REGISTRY} -u ${CIC_BUILDER_REGISTRY_USER} --password-stdin"
  for i in {1..6}; do
    if ! echo "${CIC_BUILDER_REGISTRY_PASSWORD}" | docker login ${CI_REGISTRY} -u ${CIC_BUILDER_REGISTRY_USER} --password-stdin; then
      if [ $i -eq 6 ]; then
        echo "DEBUG: Docker login failed! Issue with docker daemon connection! Exiting..."
        exit 1
      else
        echo "DEBUG: Docker login failed! trying again in 5s..."
        sleep 5
      fi
    else
      echo "DEBUG: Docker login succeeded. Continue for next step..."
      break
    fi
  done
}

function cic_builder() {
  env | grep DTC
  envsubst < dtgeo_request.json.tpl > ${CI_PROJECT_DIR}/dtgeo_request.json
  echo "Request template content:"
  cat ${CI_PROJECT_DIR}/dtgeo_request.json
  echo ""
  _USERNAME_="DTC${s#ST}_USERNAME"
  _TOKEN_="DTC${s#ST}_TOKEN"
  echo "Username: ${_USERNAME_}"
  echo "Token: ${_TOKEN_}"
  if [ -n "${!_USERNAME_}" -a -n "${!_TOKEN_}" ]; then
    echo "Username and token defined"
    echo "getting URL from command: git config -f .gitmodules submodule.DTC${w:2:2}/${w}/${s}/src.url"
    _URL_=$(git config -f .gitmodules submodule.DTC${w:2:2}/${w}/${s}/src.url)
    echo "URL for submodule: ${_URL_}"
    git config -f .gitmodules submodule.DTC${w:2:2}/${w}/${s}/src.url "https://${!_USERNAME_}:${!_TOKEN_}@${_URL_#*://}"
    echo "gitmodules modified with private repo credentials for path: DTC${w:2:2}/${w}/${s}/src"
  fi
  if [ -z "${DTC_REPO_NAME_}" ]; then
    DTC_REPO_NAME_="${DTC_ID}/${DTC_WORKFLOW_ID}/${DTC_STEP_ID}"
    echo "Set DTC_REPO_NAME variable to: ${DTC_REPO_NAME_}"
  fi
  if [ -z "${DTC_VERSION_}" ]; then
    DTC_VERSION_="latest"
    echo "Set DTC_VERSION variable to: ${DTC_VERSION}"
  fi
  if git submodule update --init --remote DTC${w:2:2}/${w}/${s}/src; then
    echo "Updated submodule: DTC${w:2:2}/${w}/${s}/src"
    pushd DTC${w:2:2}/${w}/${s}
    CONTAINER_REPO_LIST="$(curl --header "PRIVATE-TOKEN: ${DTGEO_API_TOKEN}" "${CI_API_V4_URL}/projects/${CI_PROJECT_PATH//\//%2F}/registry/repositories" 2>/dev/null)"
    echo "CONTAINER_REPO_LIST = $CONTAINER_REPO_LIST"
    CONTAINER_REPO_COUNT="$(echo "${CONTAINER_REPO_LIST}" | jq -r '. | length')"
    echo "CONTAINER_REPO_COUNT = $CONTAINER_REPO_COUNT"
    i=0
    while [ $i -lt ${CONTAINER_REPO_COUNT} ]; do
      CONTAINER_PATH=$(jq ".[$i].path" <<< ${CONTAINER_REPO_LIST})
      echo "CONTAINER_PATH = ${CONTAINER_PATH}"
      echo "Current path to be tested: ${CI_PROJECT_PATH}/${DTC_REPO_NAME_}"
      if [[ "${CONTAINER_PATH//\"/}" == "${CI_PROJECT_PATH}/${DTC_REPO_NAME_}" ]]; then
        CONTAINER_REPO_ID=$(jq -r ".[$i].id" <<< ${CONTAINER_REPO_LIST})
        echo "CONTAINER_REPO_ID = $CONTAINER_REPO_ID"
        break
      fi
      i=$((i+1))
    done
    echo "DEBUG: while loop finished"
    docker_login
    if [ ! -z "${CONTAINER_REPO_ID}" ]; then
      echo "DEBUG: inside if when container repo already exists"
      CONTAINER_IMAGE_TS_BEFORE="$(curl --header "PRIVATE-TOKEN: ${DTGEO_API_TOKEN}" "${CI_API_V4_URL}/registry/repositories/${CONTAINER_REPO_ID}/tags/${DTC_VERSION_}" 2>/dev/null | jq -r '.created_at')"
      echo "CONTAINER_IMAGE_TS_BEFORE = $CONTAINER_IMAGE_TS_BEFORE"
      echo "DEBUG: cic_builder.py call when container repo already exists"
      if python3 /image_creation/cic_builder.py --request=${CI_PROJECT_DIR}/dtgeo_request.json 2>&1 ; then
        CONTAINER_IMAGE_TS_AFTER="$(curl --header "PRIVATE-TOKEN: ${DTGEO_API_TOKEN}" "${CI_API_V4_URL}/registry/repositories/${CONTAINER_REPO_ID}/tags/${DTC_VERSION_}" 2>/dev/null | jq -r '.created_at')"
        echo "CONTAINER_IMAGE_TS_AFTER = $CONTAINER_IMAGE_TS_AFTER"
        if [[ "$CONTAINER_IMAGE_TS_BEFORE" == "$CONTAINER_IMAGE_TS_AFTER" ]]; then
          echo "cic_builder: image is the same for path DTC${w:2:2}/${w}/${s}/src"
          echo "cic_builder: failed to upload test image"
        else
          echo "cic_builder: uploaded successfully test image for path DTC${w:2:2}/${w}/${s}/src"
        fi
      else
        echo "cic_builder: failed to upload test image for path DTC${w:2:2}/${w}/${s}/src"
        exit 1
      fi
    else
      echo "DEBUG: cic_builder.py call when container repo missing"
      if python3 /image_creation/cic_builder.py --request=${CI_PROJECT_DIR}/dtgeo_request.json 2>&1 ; then
        echo "cic_builder: uploaded successfully test image for path DTC${w:2:2}/${w}/${s}/src"
      else
        echo "cic_builder: failed to upload test image for path DTC${w:2:2}/${w}/${s}/src"
        exit 1
      fi
    fi
    echo "DEBUG: before popd"
    popd
    echo "DEBUG: after popd"
  else
    echo "Failed to update submodule: DTC${w:2:2}/${w}/${s}/src"
    exit 1
  fi
}

DTC_PLATFORM_=${DTC_PLATFORM_:-'linux/amd64'}
DTC_ARCHITECTURE_=${DTC_ARCHITECTURE_:-'skylake'}
DTC_CONTAINER_ENGINE_=${DTC_CONTAINER_ENGINE_:-'docker'}
DTC_VERSION_=${DTC_VERSION_:-"$CI_COMMIT_REF_NAME"}
DTC_GIT_FORCE_FLAG_=${DTC_GIT_FORCE_FLAG_:-'true'}
DTC_GIT_PUSH_=${DTC_GIT_PUSH_:-'true'}
DTC_WORKSPACE_PATH_SUFFIX_=${DTC_WORKSPACE_PATH_SUFFIX_:-''}
DTC_DEBUG_=${DTC_DEBUG_:-'false'}

apt update
apt install -y gettext-base jq
