#!/usr/bin/env python
#!/home/louisei.cordrie/MINICONDA/miniconda3/bin/python3.8

# Import system modules
import os
import ast
import sys
import math
import scipy
import numpy       as np
import copy
import itertools
import time
from scipy.stats import truncnorm

def find_closest_coord(array, target):
    # Find the index of the element closest to the target value
    index = np.abs(array - target).argmin()
    # Get the value at that index
    closest_value = array[index]
    return closest_value, index

def generate_points_in_circle(center_x,center_y, radius, num_points):
    
    points = []
    while len(points) < num_points:
        x = np.random.uniform(center_x - radius, center_x + radius)
        y_range = np.sqrt(radius**2 - (x - center_x)**2)
        y = np.random.uniform(center_y - y_range, center_y + y_range)
        points.append((x, y))
    return points

def generate_points_with_probability(center_x, center_y, radius, num_points, grid_coord, long_term_prob_grid, short_term_prob_grid):
    
    points = []
    grid_xmin = grid_coord[2]
    grid_ymin = grid_coord[3]
    grid_xmax = grid_coord[2]+(grid_coord[4]*grid_coord[0])
    grid_ymax = grid_coord[3]+(grid_coord[5]*grid_coord[1])

    while len(points) < num_points:
        
        # Generate random offsets from a Gaussian distribution
        offsets_x = np.random.normal(loc=0, scale=radius, size=num_points)
        offsets_y = np.random.normal(loc=0, scale=radius, size=num_points)
        # Add offsets to the center position to get the final points
        points_x = center_x + offsets_x
        points_y = center_y + offsets_y
        
        # Iterate over generated points
        for x, y in zip(points_x, points_y):
            # Verification that the point is inside the topo grid
            if grid_xmin < int(x) < grid_xmax and grid_ymin < int(y) < grid_ymax:
               # Search for closest points in probability grid
               if long_term_prob_grid is not None:
                   if long_term_prob_grid[0][0] < int(x) < long_term_prob_grid[0][-1] and long_term_prob_grid[1][0] < int(y) < long_term_prob_grid[1][-1]:
                       xgrid, xind = find_closest_coord(long_term_prob_grid[0],x)
                       ygrid, yind = find_closest_coord(long_term_prob_grid[1],y)
                       probability_value = long_term_prob_grid[2][int(xind), int(yind)]
                       # Randomly select the point based on the probability value
                       if np.random.rand() < probability_value:
                           points.append((x, y))

    # Return the selected points
    return points

def position_parameter(num_points,VAR_DIC):

    # Loading coordinates
    grid_coord = VAR_DIC['simulation']['grid_coordinates']
    # Loading radius
    if VAR_DIC['short_term_data']['eruption_radius'] is not None:
        radius = VAR_DIC['short_term_data']['eruption_radius']
    else:
        radius = 500
    # Loading the probability vent opening map
    if VAR_DIC['long_term_data']['vent_opening_grid']:
        long_term_vent = VAR_DIC['long_term_data']['vent_opening_grid']
    else:
        long_term_vent = None
    # Loading the probability vent opening map
    if VAR_DIC['short_term_data']['unrest']:
        short_term_vent = VAR_DIC['short_term_data']['vent_opening_grid']
    else:
        short_term_vent = None
    # Loading short term information
    if VAR_DIC['short_term_data']['eruption_coord'] is not None:
        center_x = VAR_DIC['short_term_data']['eruption_coord'][0]
        center_y = VAR_DIC['short_term_data']['eruption_coord'][1]
    else:
        center_x = grid_coord[2]+(grid_coord[4]*grid_coord[0]/2)
        center_y = grid_coord[3]+(grid_coord[5]*grid_coord[1]/2)

    latlon = generate_points_with_probability(center_x, center_y, radius, num_points, grid_coord, long_term_vent, short_term_vent)

    return latlon

def flow_parameter(vent_num,latlon1,latlon2,VAR_DIC):
    

    if VAR_DIC['short_term_data']['average_flow_rate']:
        flow_med = VAR_DIC['short_term_data']['average_flow_rate']
    else:
        flow_med = VAR_DIC['long_term_data']['average_flow_rate']

    if VAR_DIC['short_term_data']['type_flow_rate']:
        flow_type = VAR_DIC['short_term_data']['type_flow_rate']
    else:
        flow_type = VAR_DIC['long_term_data']['type_flow_rate']

    if VAR_DIC['short_term_data']['values_flow_rate']:
        flow_values = VAR_DIC['short_term_data']['values_flow_rate']
    else:
        flow_values = VAR_DIC['long_term_data']['values_flow_rate']

    if flow_type == 'constant':
        std_dev = 2
        a, b = (0 - flow_med) / std_dev, np.inf  # Truncate at 0
        flow = truncnorm(a, b, loc=flow_med, scale=std_dev).rvs(vent_num)/vent_num
        #flow = np.random.normal(loc=flow_med, scale=2, size=vent_num)/vent_num

    return flow


def viscosity_parameter(latlon1,latlon2,VAR_DIC):

    visc_mod = VAR_DIC['long_term_data']['viscosity_model']
    min_thick_ref = float(VAR_DIC['long_term_data']['minimum_thickness'])
    
    # Thickness
    std_dev = 0.2
    a, b = (0 - min_thick_ref) / std_dev, np.inf
    min_thick = truncnorm.rvs(a, b, loc=min_thick_ref, scale=std_dev, size=1)[0]
 
    visc=np.zeros((3))

    if visc_mod == 'CONSTANT':
       visc_ref = int(VAR_DIC['long_term_data']['viscosity_constant'])
       std_dev = 200000
       a, b = (0 - visc_ref) / std_dev, np.inf
       visc[0] = truncnorm.rvs(a, b, loc=visc_ref, scale=std_dev, size=1)[0]
       #visc = np.random.normal(loc=visc_ref, scale=10000)

    elif visc_mod == 'VFT':
       visc_ref_A = int(VAR_DIC['long_term_data']['viscosity_VFT'][0])
       visc_ref_B = int(VAR_DIC['long_term_data']['viscosity_VFT'][1])
       visc_ref_C = int(VAR_DIC['long_term_data']['viscosity_VFT'][2])
       A = visc_ref_A
       B = visc_ref_B
       C = visc_ref_C
       visc[0]=A
       visc[1]=B
       visc[2]=C

    elif visc_mod == 'ARRHENIAN':
       visc_ref_A = int(VAR_DIC['long_term_data']['viscosity_ARR'][0])
       visc_ref_B = int(VAR_DIC['long_term_data']['viscosity_ARR'][1])
       A = 1
       B = 11725
       visc[0]=A
       visc[1]=B
    
    elif visc_mod == 'EXPONENTIAL':
       visc_ref_A = int(VAR_DIC['long_term_data']['viscosity_EXP'][0])
       visc_ref_B = int(VAR_DIC['long_term_data']['viscosity_EXP'][1])
       A = 63
       B = 0.0486
       visc[0]=A
       visc[1]=B

    return min_thick, visc_mod, visc

