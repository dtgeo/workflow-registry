#!/usr/bin/env python

import os
import ast
import sys
import utm
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.colors import LightSource, LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from numpy import matlib as mb
from scipy.stats import norm
import netCDF4 # import libraries
from netCDF4 import Dataset,num2date,date2num
import pathlib

# Define paths
main_folder = os.getcwd()
module_folder = os.path.join(main_folder, 'MODULES')
execution_folder = os.path.join(main_folder, 'RUN')
function_folder = os.path.join(module_folder, 'FUNCTIONS')
data_folder = os.path.join(main_folder, 'DATA')
template_folder = os.path.join(data_folder, 'TEMPLATES')
sys.path.append(module_folder)
sys.path.append(function_folder)

def plot_position_overprob(latlon,vent_num,VAR_DIC):

    fig_folder = os.path.join(execution_folder, 'FIGURES')
    os.makedirs(fig_folder, exist_ok=True)

    vent_opening_grid = VAR_DIC['long_term_data']['vent_opening_grid']
    vents_grid = vent_opening_grid[2]
    if VAR_DIC['short_term_data']['observation_grid'] is not None:
        observation_grid = VAR_DIC['short_term_data']['observation_grid']
        observation_data = Dataset(str(observation_grid), 'r')
        observation = observation_data.variables['z'][:]

    # Colormaps, ranges
    vents_grid_cmap = 'viridis'  # Example colormap for vents_grid
    vents_grid_vmin, vents_grid_vmax = 0, 1  # Example color range for vents_grid
    coord = VAR_DIC['simulation']['grid_coordinates']
    minlon = np.min(vent_opening_grid[0])
    minlat = np.min(vent_opening_grid[1])
    maxlon = np.max(vent_opening_grid[0])
    maxlat = np.max(vent_opening_grid[1])

    # Plot the combined grid
    plt.figure()
    # Plot topography with its colormap
    vents_grid_norm = Normalize(vmin=vents_grid_vmin, vmax=vents_grid_vmax)
    vents_grid_plot = plt.imshow(vents_grid, cmap=vents_grid_cmap, extent=(minlon,maxlon,minlat,maxlat),alpha=0.7,origin='lower')
    #vents_grid_plot = plt.imshow(vents_grid, cmap=vents_grid_cmap, extent=(minlon,maxlon,minlat,maxlat),alpha=0.7, norm=vents_grid_norm, origin='lower')
    #Contour plot
    if VAR_DIC['short_term_data']['observation_grid'] is not None:
        plt.contourf(observation, levels=[0,1], colors='red',alpha=0.3)
    # Bar plot
    cbar_vents_grid = plt.colorbar(vents_grid_plot,label='Vent opening probability', shrink=0.5, pad=0.05)
    plt.title('Vents position')

    #Add the position of the vents
    vent_num_1=np.where(vent_num==1)[0]
    vent_num_2=np.where(vent_num==2)[0]
    for vent in vent_num_1:
        plt.scatter(latlon[0][vent][0],latlon[0][vent][1],color='blue',s=0.05)
    for vent in vent_num_2:
        plt.scatter(latlon[0][vent][0],latlon[0][vent][1],color='orange',s=0.05)
        plt.scatter(latlon[1][vent][0],latlon[1][vent][1],color='orange',s=0.05)

    #Add the position of the eruption
    if VAR_DIC['short_term_data']['eruption_coord'] is not None:
        center_x = VAR_DIC['short_term_data']['eruption_coord'][0]
        center_y = VAR_DIC['short_term_data']['eruption_coord'][1]
        plt.scatter(center_x,center_y,color='red',s=10)

    # Set x-axis and y-axis ticks with latitude and longitude values
    plt.xlim([coord[2],coord[2]+(coord[0]*coord[4])])
    plt.ylim([coord[3],coord[3]+(coord[1]*coord[5])])
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.savefig(str(fig_folder)+'/source_positions_probmap.png', dpi=300, bbox_inches='tight')

    if VAR_DIC['short_term_data']['observation_grid'] is not None:
       observation_data.close()


def plot_position_overtopo(latlon,vent_num,VAR_DIC):

    fig_folder = os.path.join(execution_folder, 'FIGURES')
    os.makedirs(fig_folder, exist_ok=True)

    # Plot the combined grid
    plt.figure()

    #### TOPOGRAPHY ####
    topo_folder = str(data_folder)+'/LONG/TOPO/'
    topo_id = VAR_DIC['simulation']['topography_fig']
    topography_data = Dataset(str(topo_folder)+str(topo_id), 'r')
    topography = topography_data.variables['z'][:]
    if VAR_DIC['short_term_data']['observation_grid'] is not None:
        observation_grid = VAR_DIC['short_term_data']['observation_grid']
        observation_data = Dataset(str(observation_grid), 'r')
        observation = observation_data.variables['z'][:]
    # Colormaps, ranges
    topography_cmap = 'gray' #'terrain'  # Example colormap for topography
    topography_vmin, topography_vmax = 0, 3000  # Example color range for topography
    coord = VAR_DIC['simulation']['grid_coordinates']
    minlon = coord[2]
    minlat = coord[3]
    maxlon = coord[2]+(coord[0]*coord[4])
    maxlat = coord[3]+(coord[1]*coord[5])
    # Mask out 0 values in the lava flow thickness grid
    masked_topography = np.ma.masked_greater(topography, 10000)
    # Plot topography with its colormap
    topography_norm = Normalize(vmin=topography_vmin, vmax=topography_vmax)
    topography_plot = plt.imshow(masked_topography, cmap=topography_cmap, extent=(minlon,maxlon,minlat,maxlat),alpha=0.9, norm=topography_norm, origin='lower')
    # Add hillshading to the plot
    ls = LightSource(azdeg=315, altdeg=45)  # Define the light source direction
    hillshaded = ls.hillshade(masked_topography, vert_exag=0.1)  # Generate hillshading
    plt.imshow(hillshaded, cmap='gray', alpha=0.7, origin='lower',extent=(minlon,maxlon,minlat,maxlat))  # Over
    # Bar plot
    cbar_topography = plt.colorbar(topography_plot,label='Topography', shrink=0.5, pad=0.05)
    plt.title('Vents position')

    #### VENT OPENING ####
    vent_opening_grid = VAR_DIC['long_term_data']['vent_opening_grid']
    vents_grid = vent_opening_grid[2]
    if VAR_DIC['short_term_data']['observation_grid'] is not None:
        observation_grid = VAR_DIC['short_term_data']['observation_grid']
        observation_data = Dataset(str(observation_grid), 'r')
        observation = observation_data.variables['z'][:]
    # Colormaps, ranges
    vents_grid_cmap = 'viridis'  # Example colormap for vents_grid
    vents_grid_vmin, vents_grid_vmax = 0, 1  # Example color range for vents_grid
    coord = VAR_DIC['simulation']['grid_coordinates']
    minlon = np.min(vent_opening_grid[0])
    minlat = np.min(vent_opening_grid[1])
    maxlon = np.max(vent_opening_grid[0])
    maxlat = np.max(vent_opening_grid[1])
    # Plot topography with its colormap
    vents_grid_norm = Normalize(vmin=vents_grid_vmin, vmax=vents_grid_vmax)
    vents_grid_plot = plt.imshow(vents_grid, cmap=vents_grid_cmap, extent=(minlon,maxlon,minlat,maxlat),alpha=0.5,origin='lower')
    cbar_vents_grid = plt.colorbar(vents_grid_plot,label='Vent opening probability', shrink=0.5, pad=0.05)

    #### ENSEMBLE OF VENT POSITIONS ####
    vent_num_1=np.where(vent_num==1)[0]
    vent_num_2=np.where(vent_num==2)[0]
    for vent in vent_num_1:
        plt.scatter(latlon[0][vent][0],latlon[0][vent][1],color='blue',s=0.1)
    for vent in vent_num_2:
        plt.scatter(latlon[0][vent][0],latlon[0][vent][1],color='orange',s=0.1)
        plt.scatter(latlon[1][vent][0],latlon[1][vent][1],color='orange',s=0.1)

    #### POSITION OF THE ERUPTION ####
    if VAR_DIC['short_term_data']['eruption_coord'] is not None:
        center_x = VAR_DIC['short_term_data']['eruption_coord'][0]
        center_y = VAR_DIC['short_term_data']['eruption_coord'][1]
        plt.scatter(center_x,center_y,color='red',s=10)

    #### SETTING OF THE XAXIS AND YAXIS EXTENT ####
    plt.xlim([coord[2],coord[2]+(coord[0]*coord[4])])
    plt.ylim([coord[3],coord[3]+(coord[1]*coord[5])])
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.savefig(str(fig_folder)+'/source_positions.png', dpi=300, bbox_inches='tight')

    # Close the NetCDF files
    topography_data.close()
    if VAR_DIC['short_term_data']['observation_grid'] is not None:
       observation_data.close()

    return

def plot_parameters(parameters,VAR_DIC):

    fig_folder = os.path.join(execution_folder, 'FIGURES')
    os.makedirs(fig_folder, exist_ok=True)
    grid_coord = VAR_DIC['simulation']['grid_coordinates'] 
    scen_num = int(VAR_DIC['ensemble']['size'])

    # Preparation of the latlon/distance distribution
    #data1 = [parameters[line]['latlon'] for line in range(scen_num)]
    latlon = [ll for line in range(scen_num) for ll in parameters[line]['latlon']]
    lat = np.array([(latlon[i][0]) for i in range(0, len(latlon))])
    lon = np.array([(latlon[i][1]) for i in range(0, len(latlon))])
    if VAR_DIC['short_term_data']['eruption_coord'] is not None:
        ex = VAR_DIC['short_term_data']['eruption_coord'][0]
        ey = VAR_DIC['short_term_data']['eruption_coord'][1] 
    else:
        ex = grid_coord[2]+(grid_coord[4]*grid_coord[0]/2)
        ey = grid_coord[3]+(grid_coord[5]*grid_coord[1]/2)
    data1 = np.sqrt((lon - ey)**2 + (lat - ex)**2) 

    # Preparation of the parameters lists
    data2 = list([parameters[line]['vents'] for line in range(scen_num)])
    data3 = list([parameters[line]['visc'][0] for line in range(scen_num)]) #[v for line in range(scen_num) for v in parameters[line]['visc']]
    data4 = np.concatenate([parameters[line]['flow'] for line in range(scen_num)]).tolist() #[f for line in range(scen_num) for f in parameters[line]['flow']]
    data5 = list([parameters[line]['min_thick'] for line in range(scen_num)]) #[f for line in range(scen_num) for f in parameters[line]['flow']]

    # Bins
    bindata1 = np.arange(min(data1),max(data1)+1,10)
    bindata2 = np.arange(min(data2),max(data2)+1,0.5)
    bindata3 = np.arange(min(data3),max(data3)+500,500)
    bindata4 = np.arange(min(data4),max(data4)+0.5,0.5)
    bindata5 = np.arange(min(data5),max(data5)+0.1,0.1)

    # Plot the histograms
    plt.figure(figsize=(10, 6))  # Set the figure size
    
    # Plot the first histogram: latlon distance
    plt.subplot(2, 2, 1)  # Create subplot 1
    plt.hist(data1, bins=bindata1, alpha=0.5)
    plt.title('Distance to eruption location')
    
    # Plot the second histogram: number of vent
    #plt.subplot(2, 2, 2)  # Create subplot 2
    #plt.hist(data2, bins=bindata2, alpha=0.5)
    #plt.title('Number of vents')
    # Plot the second histogram: number of vent
    plt.subplot(2, 2, 2)  # Create subplot 2
    plt.hist(data5, bins=bindata5, alpha=0.5)
    plt.title('Minimum thickness')
    
    # Plot the third histogram
    plt.subplot(2, 2, 3)  # Create subplot 3
    plt.hist(data3, bins=bindata3, alpha=0.5)
    #plt.axvline(x=0, color='red', linestyle='--')
    plt.title('Viscosity distribution')
    
    # Plot the fourth histogram
    plt.subplot(2, 2, 4)  # Create subplot 4
    plt.hist(data4, bins=bindata4, alpha=0.5)
    #plt.axvline(x=0, color='red', linestyle='--')
    plt.title('Flow rate distribution')
    
    plt.tight_layout()  # Adjust the layout to prevent overlapping
    plt.savefig(str(fig_folder)+'/ensemble_parameters.png', dpi=300, bbox_inches='tight')  # Show the plot
    



