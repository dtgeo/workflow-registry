#!/usr/bin/env python

import sys
import os
import time
import argparse
import json

def parse_args():
    parser = argparse.ArgumentParser(description='Your script description')

    # Add command-line arguments
    parser.add_argument('--event_id', type=str, default='Test', help='Name of the event or the run')
    parser.add_argument('--topo', type=str, default='etna_5m', help='Name of the topographic grid without the extension')
    parser.add_argument('--ensemble_size', type=int, default=1000, help='Size of the ensemble of scenarios')
    parser.add_argument('--volcano', type=str, default='etna', help='Name of the region')
    parser.add_argument('--time_end', type=int, default='259200', help='Duration of the simulation in seconds')
    parser.add_argument('--print_step', type=int, default='3600', help='Time step for output in seconds')
    parser.add_argument('--data_update', type=str, default='no', help='Activation of the data update')
    parser.add_argument('--data_assimilation', type=str, default='no', help='Activation of the data assimilation')
    parser.add_argument('--topo_update', type=str, default='no', help='Activation of the topo update')
    parser.add_argument('--source_update', type=str, default='no', help='Activation of the source update')
    parser.add_argument('--mode', type=str, default='ensemble', help='Selection of the single mode or ensemble mode')
    parser.add_argument('--output', type=str, default='yes', help='Delivery of the output')
    parser.add_argument('--machine', type=str, default='local', help='Type of computer')
    parser.add_argument('--forecast_time', type=int, default='3600', help='Post-processing time step')
    parser.add_argument('--percentiles', type=float, default='95.0', help='Post-processing percentiles')
    parser.add_argument('--weight', type=float, default='0.01', help='Post-processing percentiles')
    parser.add_argument('--density', type=float, default=None, help='Density of the lava')
    parser.add_argument('--eruption_coord', type=float, default=None, help='Coordinates of the eruption')
    parser.add_argument('--eruption_radius', type=int, default=None, help='Radius of the area of vent positions')


    # Parse the command-line arguments
    args = parser.parse_args()

    # Read the configuration file
    with open('CONF/config.json', 'r') as config_file:
        config = json.load(config_file)

    # Update command-line arguments with values from the configuration file
    for key, value in config.items():
        setattr(args, key, value)

    return args

