#!/usr/bin/env python

import os
import ast
import sys
import utm
import shutil
import numpy as np
import xarray as xr
import pandas as pd
from scipy import linalg
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.colors import LightSource, LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from numpy import matlib as mb
from scipy.stats import norm
import netCDF4 # import libraries
from netCDF4 import Dataset,num2date,date2num
import pathlib
import subprocess
from pycompss.api.task import task
from pycompss.api.parameter import IN, OUT
from pycompss.api.api import compss_wait_on

# Define paths
main_folder = os.getcwd()
execution_folder = os.path.join(main_folder, 'RUN')
assimilation_folder = os.path.join(main_folder, 'RUN_ASSI')
function_folder = os.path.join(main_folder, 'FUNCTIONS')
data_folder = os.path.join(main_folder, 'DATA')
template_folder = os.path.join(data_folder, 'TEMPLATES')
simu_template_folder = os.path.join(template_folder, 'SIMULATION')
sys.path.append(function_folder)

from ST530103.ST530103 import main_step3
from FUNCTIONS.read_write_files import load_grid
from FUNCTIONS.management_files import list_folders_and_extract_indices 
from FUNCTIONS.read_write_files import save_dict_to_file

#################################################
################## ROUTINES #####################
#################################################

def closest_points(target_point, points_array, k=1):
    distances = np.linalg.norm(points_array - target_point, axis=1)
    closest_indices = np.argsort(distances)[:k]
    closest_points = points_array[closest_indices]
    return closest_indices, closest_point


def find_closest_grid_point(lat, lon, x, y):
    # Calculate the squared differences
    lon_grid, lat_grid = np.meshgrid(lon, lat)
    lat_diff = lat_grid - x
    lon_diff = lon_grid - y
    # Calculate the squared distance
    dist_squared = lat_diff**2 + lon_diff**2
    # Find the index of the minimum distance
    min_index = np.argmin(dist_squared)
    # Convert the flat index back to a 2D index
    lat_index, lon_index = np.unravel_index(min_index, lat_grid.shape)
    return lat_index, lon_index

@task(returns=2)
def matrix_inversion(ye, hxp, nens):
    Ri = np.diag(1.0/ye**2)
    P  = hxp@hxp.T
    P /= nens-1
    #
    Pi,rank = linalg.pinvh(P,return_rank=True)
    aa = np.allclose(P, np.dot(P, np.dot(Pi,P)))
    print("Checking successful inversion:", aa)
    return Ri, Pi

@task(returns=3)
def iterative_procedure(hx, Ri, Pi, hxm, y, NT, nens):
    Q = hx.T @ (Ri + Pi) @ hx
    b = -1 * hx.T @ (Pi @ hxm + Ri @ y)
    Ap = np.abs(Q) + Q
    An = np.abs(Q) - Q
    
    w = np.ones(nens) / (1.0*nens)
    J1 = []
    J2 = []
    
    for i in range(NT):
        a = Ap @ w
        c = An @ w
        f = (np.sqrt(b**2 + a*c) - b) / a
        if np.allclose(w * f, w):
            break
        elif i == NT - 1:
            error = np.linalg.norm(w - w * f)
            print("WARN: Final error:", error)
        w *= f
        J1.append( (hx@w-hxm).T@Pi@(hx@w-hxm) )
        J2.append( (hx@w-y).T@Ri@(hx@w-y) )

    w = w / np.sum(w)
    return w, J1, J2

def gnc_calc(VAR_DIC,ENS_DIC,obsi,time_simu,list_scen):

    # Parameters
    remove_logbias = False
    plot_cost      = False
    out_table      = False
    out_factors    = False
    out_netcdf     = True

    # Results Variables
    stime_tmp = round(int(time_simu)/float(VAR_DIC['simulation']['print_step']))
    stime = max(1,stime_tmp)

    # Observation file #
    obslat = np.array(VAR_DIC['short_term_data']['lava_shape'][obsi]['lat'])
    obslon = np.array(VAR_DIC['short_term_data']['lava_shape'][obsi]['lon'])
    obsval = np.array(VAR_DIC['short_term_data']['lava_shape'][obsi]['z'])
    nobs = len(obsval)
    error = np.ones((nobs))

    # Read model data
    fname_ens_assi = str(assimilation_folder)+'/output_full_'+str(time_simu)+'.nc'
    if os.path.isfile(fname_ens_assi):
       ds = xr.open_dataset(fname_ens_assi)
       ens = ds['ens_mem']
       lat_values = ds['lat'].values
       lon_values = ds['lon'].values
       existing_members = [x for x in list_scen if x in ds['ens_mem'].values]
       missing_members = [x for x in list_scen if x not in ds['ens_mem'].values]
    else:
       fname_ens_init = str(execution_folder)+'/output_full_'+str(time_simu)+'.nc'
       ds = xr.open_dataset(fname_ens_init)
       ens = ds['ens_mem']
       lat_values = ds['lat'].values
       lon_values = ds['lon'].values
       existing_members = list_scen
       missing_members = []

    ens_new = xr.Dataset( data_vars={'existing_mem': (['index'], existing_members)},
                                        coords={'ens': (['ens'], existing_members)})
    lava_shape_new = ds['lava_shape'].sel(ens=existing_members)
    lava_thick_new = ds['lava_thick'].sel(ens=existing_members)
    lava_temp_new = ds['lava_temp'].sel(ens=existing_members)

    if len(missing_members)>0:
        fname_ens_init = str(execution_folder)+'/output_full_'+str(time_simu)+'.nc'
        ds_init = xr.open_dataset(fname_ens_init)
        ens_init = ds_init['ens_mem'].values
        common_members = np.intersect1d(missing_members, ens_init)
        common_members_da = xr.Dataset( data_vars={'common_mem': (['index'], common_members)},
                                        coords={'ens': (['ens'], common_members)})
        if len(common_members)>0 and len(existing_members)>0:
           lava_shape_init = ds_init['lava_shape'].sel(ens=common_members)
           lava_thick_init = ds_init['lava_thick'].sel(ens=common_members)
           lava_temp_init = ds_init['lava_temp'].sel(ens=common_members)
           ens_new = xr.concat([ens_new['existing_mem'], common_members_da['common_mem']],dim='index')
           lava_shape_new = xr.concat([lava_shape_new, lava_shape_init], dim='ens')
           lava_thick_new = xr.concat([lava_thick_new, lava_thick_init], dim='ens')
           lava_temp_new = xr.concat([lava_temp_new, lava_temp_init], dim='ens')
           ens_new_val = ens_new.values
        elif len(common_members)>0:
           ens_new_val = common_members_da['common_mem'].values
           lava_shape_new = ds_init['lava_shape'].sel(ens=common_members)
           lava_thick_new = ds_init['lava_thick'].sel(ens=common_members)
           lava_temp_new = ds_init['lava_temp'].sel(ens=common_members)
    else:
        ens_new_val = list_scen
        lava_shape_new = ds['lava_shape'].sel(ens=list_scen)
        lava_thick_new = ds['lava_thick'].sel(ens=list_scen)
        lava_temp_new = ds['lava_temp'].sel(ens=list_scen)

    ds_new = xr.Dataset( data_vars={'ens_mem': (['index'], ens_new_val),
                      'lava_shape': (('ens','lat','lon'), lava_shape_new.values),
                      'lava_thick': (('ens','lat','lon'), lava_thick_new.values),
                      'lava_temp': (('ens','lat','lon'), lava_temp_new.values),},
               coords={'ens': (['ens'], ens_new_val),
                             'lat': (['lat'], lat_values),
                             'lon': (['lon'], lon_values)})

    list_scen = ds_new['ens_mem'].values.astype(int)
    nens = len(list_scen)

    # Prep
    hx  = np.zeros((nobs,nens))
    hxp = np.zeros((nobs,nens))
    y   = np.zeros(nobs)
    ye  = np.zeros(nobs)

    # Interpolation
    for i in range(nobs):
        lat   = obslat[i]
        lon   = obslon[i]
        y[i]  = obsval[i] #Observation in cm
        ye[i] = error[i]
        ### Interpolate
        hx[i,:] = ds_new['lava_shape'].sel(ens=list_scen).interp(lat=lat,lon=lon).values
        hx[i,:] = np.nan_to_num(hx[i,:], nan=0.0)        
    
    hxm = np.mean(hx,axis=1)
    for i in range(nens):
        hxp[:,i] = hx[:,i] - hxm

    print("Before inversion")
    # COMPSs: Matrix Inversion
    Ri, Pi = matrix_inversion(ye, hxp, nens)
    Ri = compss_wait_on(Ri)
    Pi = compss_wait_on(Pi)
    
    NT = 40000
    w, J1, J2 = iterative_procedure(hx, Ri, Pi, hxm, y, NT, nens)
    w = compss_wait_on(w)
    J1 = compss_wait_on(J1)
    J2 = compss_wait_on(J2)

    # Remove log bias
    if remove_logbias:
        y_tmp = y/(hx@w)
        k=np.mean(np.log(y_tmp[y_tmp>0]))
        k=np.exp(k)
        #print("Factor k: ",k)
        w *= k
    
    # Write factor weigths
    if out_factors:
        with open('weights.dat','w') as f:
            for item in w:
                f.write("{:.9E}\n".format(item))

    if out_netcdf:

        t = xr.DataArray(w, coords=[ds_new['lava_shape'].ens.sel(ens=list_scen)],dims=["ens"])
        ds_out = xr.Dataset()
        ds_out['weights']  = w
        ds_out['analysis'] = ds_new['lava_shape'].sel(ens=list_scen).dot(t)
        ds_out['analysis_thick'] = ((ds_new['lava_thick'].sel(ens=list_scen)*ds_new['lava_shape'].sel(ens=list_scen))*t).sum(dim='ens')
        ds_out['analysis_temp'] = ((ds_new['lava_temp'].sel(ens=list_scen)*ds_new['lava_shape'].sel(ens=list_scen))*t).sum(dim='ens')
        #ds_out['forecast'] = ds_new['lava_shape'].sel(ens=list_scen).mean(dim='ens')
        ke = 3
        i=0

        for iout in list_scen:
             
            blend_par = (1-w[i])**(ke) #0.5 #(1-w[i])
            i = i+1

            # THICKNESS
            ds_out_post = (1 - blend_par)*ds_new['lava_shape'].sel(ens=iout)+blend_par*ds_out['analysis']
            non_zero_mask = ds_out_post != 0
            grid_non_zero_thick = xr.where(non_zero_mask, ((1 - blend_par)*ds_new['lava_thick'].sel(ens=iout)+blend_par*ds_out['analysis_thick']), 0)
            # TEMPERATURE
            temp_temp = (1 - blend_par)*(ds_new['lava_temp'].sel(ens=iout)-298)+blend_par*(ds_out['analysis_temp']-298)
            temp_mask = temp_temp > 0
            temp_corr = xr.where(temp_mask,temp_temp,0)
            grid_non_zero_temp = xr.where(non_zero_mask,temp_corr, 0)+298
            # SAVING
            ds_out[str(iout)+'_thick_pre'] = ds_new['lava_thick'].sel(ens=iout)
            ds_out[str(iout)+'_thick_post'] = grid_non_zero_thick
            ds_out[str(iout)+'_temp_pre'] = ds_new['lava_temp'].sel(ens=iout)
            ds_out[str(iout)+'_temp_post'] = grid_non_zero_temp

            grid=ds_out[str(iout)+'_thick_post'].values
            lon=ds_new['lon'].values
            lat=ds_new['lat'].values
            totlon = np.tile(lon, grid.shape[0])
            totlat = np.repeat(lat, grid.shape[1])
            flattened_grid = grid.flatten()
        
        ds_out.to_netcdf(str(execution_folder)+"/output_gnc_"+str(time_simu)+".nc")
   
    return ds_out, ds_new['lon'].values, ds_new['lat'].values


def create_folder(iscen):

    # Create folder
    new_scen_folder = os.path.join(assimilation_folder, 'SCEN_'+str(iscen))
    os.makedirs(new_scen_folder, exist_ok=True)
    old_scen_folder = os.path.join(execution_folder, 'SCEN_'+str(iscen))

    return old_scen_folder, new_scen_folder

def create_inp(iscen,para,scen_folder,VAR_DIC,time_simu):

    event_id = VAR_DIC['simulation']['event_id']
    topo_id = VAR_DIC['simulation']['topography']
    topo_folder = str(data_folder)+'/LONG/TOPO/'
    visc=para['visc']
    min_thick=para['min_thick']
    visc_mod=para['visc_mod']

    num_time = round(int(time_simu)/float(VAR_DIC['simulation']['print_step']))
    num_time = max(1,num_time)

    with open(str(simu_template_folder)+'/template.inp','r') as f:
       contents = f.read()
    f.close()
    contents = contents.replace('TOPOGRAPHY = topo.grd','TOPOGRAPHY = '+str(topo_folder)+str(topo_id))
    contents = contents.replace('SOURCE     = event.src','SOURCE     = '+str(event_id)+'_'+str(iscen)+'.src')
    contents = contents.replace('#RESTART_FROM_FILE = temp','RESTART_FROM_FILE = '+str(num_time))
    contents = contents.replace('NX = temp','NX = '+str(VAR_DIC['simulation']['grid_coordinates'][0]))
    contents = contents.replace('NY = temp','NY = '+str(VAR_DIC['simulation']['grid_coordinates'][1]))
    contents = contents.replace('XMIN = temp','XMIN = '+str(VAR_DIC['simulation']['grid_coordinates'][2]))
    contents = contents.replace('YMIN = temp','YMIN = '+str(VAR_DIC['simulation']['grid_coordinates'][3]))
    contents = contents.replace('DX = temp','DX = '+str(VAR_DIC['simulation']['grid_coordinates'][4]))
    contents = contents.replace('DY = temp','DY = '+str(VAR_DIC['simulation']['grid_coordinates'][5]))
    contents = contents.replace('PRINT_STEP = temp','PRINT_STEP = '+str(VAR_DIC['simulation']['print_step']))
    contents = contents.replace('TIME_END = temp','TIME_END = '+str(VAR_DIC['simulation']['time_end']))
    contents = contents.replace('#TIME_START = temp','TIME_START = '+str(time_simu))
    
    contents = contents.replace('VISCOSITY_MODEL = temp','VISCOSITY_MODEL = '+str(visc_mod))
    contents = contents.replace('MINIMUM_THICKNESS = temp', 'MINIMUM_THICKNESS = '+str(min_thick))

    if visc_mod == 'CONSTANT':
       contents = contents.replace('VISCOSITY = temp','VISCOSITY = '+str(visc[0]))
       contents = contents.replace('VISCOSITY_PARAMETER_A = temp','#VISCOSITY_PARAMETER_A = temp')
       contents = contents.replace('VISCOSITY_PARAMETER_B = temp','#VISCOSITY_PARAMETER_B = temp')
       contents = contents.replace('VISCOSITY_PARAMETER_C = temp','#VISCOSITY_PARAMETER_C = temp')
    else:
       contents = contents.replace('VISCOSITY = temp','#VISCOSITY = temp')
       contents = contents.replace('VISCOSITY_PARAMETER_A = temp','VISCOSITY_PARAMETER_A = '+str(visc[0]))
       contents = contents.replace('VISCOSITY_PARAMETER_B = temp','VISCOSITY_PARAMETER_B = '+str(visc[1]))
       contents = contents.replace('VISCOSITY_PARAMETER_C = temp','VISCOSITY_PARAMETER_C = '+str(visc[2]))

    with open(str(scen_folder)+'/'+str(event_id)+'_'+str(iscen)+'.inp','w') as f:
       f.write(contents)

def create_src(iscen,para,scen_folder,VAR_DIC):

    event_id = VAR_DIC['simulation']['event_id']
    vents=para['vents']
    latlon=para['latlon']
    flow=para['flow']
    t0=0
    t1=VAR_DIC['simulation']['time_end']
    wi=10
    temp=np.mean(np.array(VAR_DIC['long_term_data']['lava_temperature']))

    if vents==1:

        with open(str(simu_template_folder)+'/template_v1.src','r') as f:
           contents = f.read()
        f.close()
        contents = contents.replace('t0 t1 lat lon wi flow temp',str(t0)+' '+str(t1)+' '+str(latlon[0][0])+' '+str(latlon[0][1])+' '+str(wi)+' '+str(flow[0])+' '+str(temp))
        with open(str(scen_folder)+'/'+str(event_id)+'_'+str(iscen)+'.src','w') as f:
           f.write(contents)

    elif vents==2:

        with open(str(simu_template_folder)+'/template_v2.src','r') as f:
           contents = f.read()
        f.close()
        contents = contents.replace('t0 t1 lat1 lon1 wi flow1 temp',str(t0)+' '+str(t1)+' '+str(latlon[0][0])+' '+str(latlon[0][1])+' '+str(wi)+' '+str(flow[0])+' '+str(temp))
        contents = contents.replace('t0 t1 lat2 lon2 wi flow2 temp',str(t0)+' '+str(t1)+' '+str(latlon[1][0])+' '+str(latlon[1][1])+' '+str(wi)+' '+str(flow[1])+' '+str(temp))
        with open(str(scen_folder)+'/'+str(event_id)+'_'+str(iscen)+'.src','w') as f:
           f.write(contents)

def create_job(iscen,para,scen_folder,VAR_DIC,vlava_bin):

    event_id = VAR_DIC['simulation']['event_id']
    machine = VAR_DIC['simulation']['machine']

    if machine=='cluster':
       with open(str(simu_template_folder)+'/job_ada.sh','r') as f:
          contents = f.read()
       f.close()
       contents = contents.replace('#SBATCH --error=Error_Event.txt','#SBATCH --error=Error_'+str(event_id)+'_'+str(iscen)+'.txt')
       contents = contents.replace('#SBATCH --output=Output_Event.txt','#SBATCH --output=Output_'+str(event_id)+'_'+str(iscen)+'.txt')
       contents = contents.replace('srun --ntasks=1 --cpus-per-task=1 vlava event.inp','srun --ntasks=1 --cpus-per-task=1 '+str(vlava_bin)+' '+str(event_id)+'_'+str(iscen)+'.inp')
       with open(str(scen_folder)+'/job_'+str(iscen)+'.sh','w') as f:
          f.write(contents)

    elif machine=='local':
       with open(str(simu_template_folder)+'/job_local.sh','r') as f:
          contents = f.read()
       f.close()
       contents = contents.replace('vlava event.inp',str(vlava_bin)+' '+str(event_id)+'_'+str(iscen)+'.inp')
       with open(str(scen_folder)+'/job_'+str(iscen)+'.sh','w') as f:
          f.write(contents)

def create_restart_grid(iscen,time_simu,grid_thick,grid_temp,lon,lat,new_scen_folder,VAR_DIC):

    name=['thick','temp']
    stime = round(int(time_simu)/float(VAR_DIC['simulation']['print_step']))
    stime = max(1,stime)
    coord = VAR_DIC['simulation']['grid_coordinates']
    n_lon = int(coord[0])
    n_lat = int(coord[1])
    d_lon = int(coord[4])
    d_lat = int(coord[5])
    minlon = coord[2]+d_lon/2
    minlat = coord[3]+d_lat/2
    maxlon = minlon+((n_lon-1)*d_lon)
    maxlat = minlat+((n_lat-1)*d_lat)

    for iname in name:

        if iname=='thick':
            grid=grid_thick.values
        else:
            grid=grid_temp.values
        # Flatten the grid
        flattened_grid = grid.flatten()
        lon_lat_values = np.zeros((len(flattened_grid), 3))
        lon_lat_values[:, 0] = np.tile(lon, grid.shape[0])
        lon_lat_values[:, 1] = np.repeat(lat, grid.shape[1])
        lon_lat_values[:, 2] = flattened_grid
        
        #Write grid
        txt_filename = str(new_scen_folder)+'/'+str(VAR_DIC['simulation']['event_id'])+'_'+str(iscen)+'_'+str(iname)+'_'+str(stime).rjust(5,'0')+'.txt'
        surfer_filename = str(new_scen_folder)+'/'+str(VAR_DIC['simulation']['event_id'])+'_'+str(iscen)+'_'+str(iname)+'_'+str(stime).rjust(5,'0')+'.grd'
        np.savetxt(txt_filename, lon_lat_values, fmt='%.8f %.8f %.8f')
        gmt='/usr/bin/gmt'
        #line='grdconvert '+str(txt_filename)+' -R'+str(minlon)+'/'+str(maxlon)+'/'+str(minlat)+'/'+str(maxlat)+' -I'+str(d_lon)+'=/'+str(d_lat)+'= -G'+str(surfer_filename)+'=sf'
        line = ['xyz2grd',
               txt_filename,
               '-R' + str(minlon) + '/' + str(maxlon) + '/' + str(minlat) + '/' + str(maxlat),
               '-I' + str(d_lon) + '=/' + str(d_lat) + '=',
               '-G' + str(surfer_filename) + '=sf']
        
        #Write in folder
        os.chdir(new_scen_folder)
        process = subprocess.Popen([gmt] + line, stdout=subprocess.PIPE)
        process.wait()
        os.chdir(main_folder)

    return process


#################################################
#################### MAIN #######################
#################################################

def main_mod2(VAR_DIC,ENS_DIC,vlava_bin,obsi,time_simu,list_scen):

    # Prints
    print("          ### Entering Mod2: data assimilation ###          ")

    # GNC data assimilation
    print(" # Data assimilation #")
    print("Selected scenarios: ",list_scen)
    grids_assi, lon, lat = gnc_calc(VAR_DIC,ENS_DIC,obsi,time_simu,list_scen)

    # Save old simulations and create new set of folders
    print(" # Storing old simulations #")
    if obsi>0:
        old_scen_folder = assimilation_folder+'/ASSI_T'+str(VAR_DIC['short_term_data']['lava_shape'][obsi-1]['time'])
        os.makedirs(old_scen_folder, exist_ok=True)
        # Iterate over all items in the folder
        for item in os.listdir(assimilation_folder):
            # Check if the item is a directory
            if os.path.isdir(os.path.join(assimilation_folder, item)) and "SCEN" in item:
                # Move the subfolder to the destination folder
                shutil.move(os.path.join(assimilation_folder,item), old_scen_folder)

    # Create vlava input files
    print(" # Ceating folder structure for new simulations #")
    list_scen = list_scen.astype(int)
    for iscen in list_scen:
        old_scen_folder, new_scen_folder = create_folder(iscen)
        create_inp(iscen,ENS_DIC[iscen],new_scen_folder,VAR_DIC,time_simu)
        create_src(iscen,ENS_DIC[iscen],new_scen_folder,VAR_DIC)
        create_job(iscen,ENS_DIC[iscen],new_scen_folder,VAR_DIC,vlava_bin)
        create_restart_grid(iscen,time_simu,grids_assi[str(iscen)+'_thick_post'],grids_assi[str(iscen)+'_temp_post'],lon,lat,new_scen_folder,VAR_DIC)

    print('# New simulations : ',list_scen)

    VAR_DIC['assimilation']={}
    VAR_DIC['assimilation']['size'] = len(list_scen) 
    VAR_DIC['assimilation']['list_scen'] = list_scen
    main_step3(VAR_DIC,'assimilation')

    return

