#!/usr/bin/env python

import os
import ast
import sys
import utm
import numpy as np
import xarray as xr
import pandas as pd
from scipy.stats import expon
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.colors import LightSource, LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from numpy import matlib as mb
from scipy.stats import norm
import netCDF4 # import libraries
from netCDF4 import Dataset,num2date,date2num
import pathlib

# Define paths
main_folder = os.getcwd()
execution_folder = os.path.join(main_folder, 'RUN')
function_folder = os.path.join(main_folder, 'FUNCTIONS')
data_folder = os.path.join(main_folder, 'DATA')
template_folder = os.path.join(data_folder, 'TEMPLATES')
sys.path.append(function_folder)

from FUNCTIONS.read_write_files import load_grid

#################################################
################## ROUTINES #####################
#################################################

def closest_points(target_point, points_array, k=1):
    distances = np.linalg.norm(points_array - target_point, axis=1)
    closest_indices = np.argsort(distances)[:k]
    closest_points = points_array[closest_indices]
    return closest_indices, closest_points

def weighted_percentile(stacked_data, perc, weights):
    """
    Compute the weighted percentile along a specified axis for a stacked array of matrices.

    Parameters:
    - stacked_data: 3D array where the percentile is calculated along a specified axis.
    - perc: The percentile (0-100) to compute.
    - weights: 1D array of weights corresponding to each matrix in the stack.
    - axis: The axis along which to compute the percentile (default is 0).

    Returns:
    - Percentile values along the specified axis.
    """
    axis = 0
 
    # Flatten all other axes except the first
    flat_data = stacked_data.reshape(stacked_data.shape[0], -1)
    
    # Sort data and apply weights
    sorted_indices = np.argsort(flat_data, axis=0)
    sorted_data = np.take_along_axis(flat_data, sorted_indices, axis=0)
    sorted_weights = np.take_along_axis(np.broadcast_to(weights[:, None], flat_data.shape), sorted_indices, axis=0)

    # Compute the cumulative distribution function (CDF)
    cum_weights = np.cumsum(sorted_weights, axis=0)
    cum_weights /= cum_weights[-1, :]  # Normalize to 1

    # Interpolate to find the percentile
    percentile_values = np.empty_like(sorted_data[0, :])
    for i in range(sorted_data.shape[1]):
        percentile_values[i] = np.interp(perc / 100.0, cum_weights[:, i], sorted_data[:, i])

    # Reshape the result back to the original shape, minus the axis
    new_shape = stacked_data.shape[1:]  # Original shape minus the stacked dimension
    return percentile_values.reshape(new_shape)


def probability_update(VAR_DIC,ENS_DIC,time_simu,list_scen):

    if VAR_DIC['update']['update_status'] is not None:
        
        # status of the last udate
        status = VAR_DIC['update']['update_status']
        W = 0.1 #float(VAR_DIC['update']['weight'])

        # values of Jaccard indices J1
        J1 = [ENS_DIC[item]['jaccard_ind_1'][str(time_simu)] for item in list_scen]
        J1exp = np.array([1 - value for value in J1])
        # weight of the metric
        weights1 = expon.pdf(J1exp,loc=0,scale=W)
        weights1 /= np.sum(weights1)
        
        # values of Jaccard indices J2
        J2 = [ENS_DIC[item]['jaccard_ind_2'][str(time_simu)] for item in list_scen]
        J2exp = np.array([1 - value for value in J2])
        # weight of the metric
        weights2 = expon.pdf(J2exp,loc=0,scale=W)
        weights2 /= np.sum(weights2)

        # values of Maximal distance indices MDI
        MDI = [ENS_DIC[item]['mdi_ind'][str(time_simu)] for item in list_scen]
        # weight of the metric
        weights3 = expon.pdf(MDI,loc=0,scale=0.1)
        weights3 /= np.sum(weights3)
        
        # Merging J1 and MDI
        weights4 = 0.8*weights1+0.2*weights3
        weights4 /= np.sum(weights4)

    else:

        status = 'None'
        weights1 = np.ones(len(ENS_DIC))
        weights1 /= np.sum(weights1)
        weights2 = np.ones(len(ENS_DIC))
        weights2 /= np.sum(weights2)
        weights3 = np.ones(len(ENS_DIC))
        weights3 /= np.sum(weights3)

    return weights1, weights2, status

def forecast_calculation(VAR_DIC,weights,status,time_simu,list_scen):

    grid_files = {}
    forecast_grids = {}

    # Loading the scenario's vlava grids
    fname_ens_init = str(execution_folder)+'/output_full_'+str(time_simu)+'.nc'
    ds = xr.open_dataset(fname_ens_init)
    grid_files[str(time_simu)] = {}
    grid_files[str(time_simu)] = [ds['lava_thick'].sel(ens=iscen).values for iscen in list_scen]
    
    # Calculating the means and percentiles
    forecast_grids[str(time_simu)] = {}
    # Mean
    weighted_grids = [weight*grid for weight,grid in zip(weights,grid_files[str(time_simu)])]
    stacked_grids = np.stack(weighted_grids, axis=0)
    mean_grid = np.sum(stacked_grids, axis=0)
    forecast_grids[str(time_simu)]['mean'] = mean_grid

    # Percentiles
    stacked_grids_perc = np.stack(grid_files[str(time_simu)], axis=0)
    for percentile in VAR_DIC['output']['percentiles']:
        perc = float(percentile)
        #perc_grid = np.percentile(stacked_grids_perc, perc, axis=0,method='inverted_cdf',weights=weights)
        perc_grid = weighted_percentile(stacked_grids, perc, weights)
        forecast_grids[str(time_simu)][str(percentile)] = perc_grid
    
    return forecast_grids

def plot_grid(VAR_DIC,grid,plot_type,name):

    fig_folder = os.path.join(execution_folder, 'FIGURES')
    # Identification of the coordinates and resolution
    coord = VAR_DIC['simulation']['grid_coordinates']
    minlon = coord[2]
    minlat = coord[3]
    n_lon = int(coord[0])
    n_lat = int(coord[1])
    d_lon = int(coord[4])
    d_lat = int(coord[5])
    maxlon = minlon+((n_lon-1)*d_lon)
    maxlat = minlat+((n_lat-1)*d_lat)
    lon = np.arange(minlon,maxlon+1,d_lon)
    lat = np.arange(minlat,maxlat+1,d_lat)

    # Preparation of netcdf grids
    topo_id = VAR_DIC['simulation']['topography_fig']
    topo_folder = str(data_folder)+'/LONG/TOPO/'
    lava_folder = str(data_folder)+'/SHORT/SAT/'
    topography_data = Dataset(str(topo_folder)+str(topo_id), 'r')
    topography = topography_data.variables['z'][:]
    if VAR_DIC['short_term_data']['observation_grid'] is not None:
        observation_grid = VAR_DIC['short_term_data']['observation_grid']

    # Colormaps, ranges
    lava_flow_cmap = 'gray'  # Example colormap for lava flow
    topography_cmap = 'terrain'  # Example colormap for topography
    lava_flow_vmin, lava_flow_vmax = 0, 10  # Example color range for lava flow
    topography_vmin, topography_vmax = 0, 3000  # Example color range for topography

    # Attribution of x
    x=np.array(grid)
    obs=observation_grid.reshape((n_lat,n_lon))
    thickness = x.reshape((n_lat,n_lon))
    # Mask out 0 values in the lava flow thickness grid
    masked_topography = np.ma.masked_greater(topography, 10000)
    masked_thickness = np.ma.masked_less_equal(thickness, 0.01)
    combined_grid = masked_topography + thickness

    # Plot the combined grid
    plt.figure()
    # Plot topography with its colormap
    topography_norm = Normalize(vmin=topography_vmin, vmax=topography_vmax)
    topography_plot = plt.imshow(masked_topography, cmap=topography_cmap, alpha=0.7, norm=topography_norm, origin='lower')
    # Add hillshading to the plot
    ls = LightSource(azdeg=315, altdeg=45)  # Define the light source direction
    hillshaded = ls.hillshade(masked_topography, vert_exag=0.1)  # Generate hillshading
    plt.imshow(hillshaded, cmap='gray', alpha=0.7, origin='lower')  # Over
    #Contour plot
    if VAR_DIC['short_term_data']['observation_grid'] is not None:
       plt.contourf(obs, levels=[0.5, 1], colors='blue',alpha=0.2)
    # Plot lava flow with its colormap
    lava_flow_norm = Normalize(vmin=lava_flow_vmin, vmax=lava_flow_vmax)
    if masked_thickness.max()>0.01:
       lava_flow_norm = LogNorm(vmin=masked_thickness.min(), vmax=masked_thickness.max())
    lava_flow_plot = plt.imshow(masked_thickness, cmap=lava_flow_cmap, alpha=0.9, norm=lava_flow_norm, origin='lower')
    # Bar plot
    cbar_topography = plt.colorbar(topography_plot,label='Topography', shrink=0.5, pad=0.05)
    cbar_lava_flow = plt.colorbar(lava_flow_plot,label='Lava thickness', shrink=0.5, pad=0.01)
    plt.title('Lava flow '+str(plot_type)+' thickness above topography')

    # Set x-axis and y-axis ticks with latitude and longitude values
    latitu = np.arange(minlat, maxlat, 2000) # Array with ticks
    latitudes = np.arange(minlat, maxlat, d_lat) # Array with real grid resolution
    longitu = np.arange(minlon, maxlon, 2000)
    longitudes = np.arange(minlon, maxlon, d_lat)
    ytemp=list(np.linspace(0,len(latitudes),len(latitu)))
    yname=list(latitu)
    xtemp=list(np.linspace(0,len(longitudes),len(longitu)))
    xname=list(longitu)
    plt.yticks(ytemp,yname)
    plt.xticks(xtemp,xname)
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.savefig(str(fig_folder)+str(name), dpi=300, bbox_inches='tight')

    # Close the NetCDF files
    topography_data.close()


def creation_grid(VAR_DIC,forecast_grids,time_simu,obsi,time_obs,list_scen,weights,J,aggregation,n):
   
    # Aggregation or best scenario

    if aggregation==True:

        # Loop
        plot_types = ['mean']
        for perc in VAR_DIC['output']['percentiles']: 
            plot_types.append(str(perc))
   
        for plot_type in plot_types:
            grid = forecast_grids[str(time_simu)][str(plot_type)]
            name = '/lava_flow_update_'+str(J)+'_T'+str(time_simu)+'_O'+str(obsi)+'_'+str(time_obs)+'_'+str(plot_type)+'_grid_'+str(len(list_scen))+'scen.png'
            plot_grid(VAR_DIC,grid,plot_type,name)

    else:

         # Searching for the best scenarios
         top_indices = np.argsort(weights)[-n:]
         best_scen = list_scen[top_indices]
         # Loading the scenario's vlava grids
         fname_ens_init = str(execution_folder)+'/output_full_'+str(time_simu)+'.nc'
         ds = xr.open_dataset(fname_ens_init)
         plot_type = 'scenario'         

         for iscen in best_scen:
             grid = ds['lava_thick'].sel(ens=iscen).values
             name = '/lava_flow_update_'+str(J)+'_T'+str(time_simu)+'_O'+str(obsi)+'_'+str(time_obs)+'_scen_'+str(iscen)+'.png'
             plot_grid(VAR_DIC,grid,plot_type,name)

    return

def main_mod1(VAR_DIC,ENS_DIC,obsi,time_obs,time_simu,list_scen):

    # Prints
    print("          ### Entering Mod1: probability update ###          ")
    n=3 # Creation of lava flow figures for indivudual scenarios, n = numbre of scenario to be printed

    # Post-processing VLAVA output
    weights1, weights2, status = probability_update(VAR_DIC,ENS_DIC,time_simu,list_scen)
 
    # Forecast calculation for J1
    forecast_grids_J1 = forecast_calculation(VAR_DIC,weights1,status,time_simu,list_scen) 
    creation_grid(VAR_DIC,forecast_grids_J1,time_simu,obsi,time_obs,list_scen,weights1,'J1',True,n)

    # Forecast calculation for J2
    forecast_grids_J2 = forecast_calculation(VAR_DIC,weights2,status,time_simu,list_scen)
    creation_grid(VAR_DIC,forecast_grids_J2,time_simu,obsi,time_obs,list_scen,weights2,'J2',True,n)

    # Plot of the 5 best scenarios
    creation_grid(VAR_DIC,forecast_grids_J1,time_simu,obsi,time_obs,list_scen,weights1,'J1',False,n)

    return

