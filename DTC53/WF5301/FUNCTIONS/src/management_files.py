import os
import numpy as np

def get_files(folder_path, string):

    # Get a list of all files in the folder
    files = os.listdir(folder_path)
    
    # Initialize a list to store filenames containing the string
    matching_files = []
    
    # Loop through the list of files
    for file in files:
        # Check if the string is present in the filename
        if string in file:
            # If found, append the filename to the list
            matching_files.append(file)
    
    # Return the list of matching filenames
    return matching_files


def list_folders_and_extract_indices(folder_path,stime):
    # Initialize lists to store folder names and indices
    folder_names = []
    indices = []

    # Iterate over all items in the folder
    for item in os.listdir(folder_path):
        # Check if the item is a directory
        if os.path.isdir(os.path.join(folder_path, item)) and "SCEN" in item:
            cancel_file=os.path.join(folder_path,item)+'/Simulation_Cancelled.txt'
            if os.path.isfile(cancel_file):
               last_time = np.loadtxt(cancel_file,dtype=int)
               #print('Times',int(last_time),int(stime))
               if int(last_time)>=int(stime):
                  folder_names.append(item)
                  index_str = ''.join(filter(str.isdigit, item[::-1]))
                  if index_str:
                     indices.append(int(index_str[::-1]))
            else: 
               folder_names.append(item)
               # Extract index from the folder name (assuming index is at the beginning)
               index_str = ''.join(filter(str.isdigit, item[::-1]))
               if index_str:
                   indices.append(int(index_str[::-1]))

    # Count the number of folders
    num_folders = len(folder_names)

    return num_folders, folder_names, indices
