import os
import numpy as np
import pickle
import json
from netCDF4 import Dataset

def isolate_string(input_string, start_char, end_char):
    start_index = input_string.find(start_char)
    if start_index == -1:
        return None  # start_char not found in the input string
    end_index = input_string.find(end_char, start_index + 1)
    if end_index == -1:
        return None  # end_char not found in the input string after start_char
    return input_string[start_index + 2:end_index]

def extract_json(file, string, jtype):
    with open(file, 'r') as f:
        json_data = json.load(f)
    if not isinstance(string, str):
       string = str(string)
    data = json_data[string] #json.loads(json_data[string])
    data = np.array(data,dtype=jtype)
    return data

def read_lonlatz(file):

    ## Read file ##
    lat=[]
    lon=[]
    z=[]
    myfile = open(file,'r')
    for line in myfile:
           col = line.split(',')
           lon.append(float(col[0]))
           lat.append(float(col[1]))
           z.append(float(col[2]))
    myfile.close()

    lat=np.array(lat)
    lon=np.array(lon)
    z=np.array(z)
    time=isolate_string(file,"_T",".")

    return time, lon, lat, z

def read_topo(file):

    ## Read coordinates file ##
    coord = []
    myfile = open(file,'r')
    for line in myfile:
        coord.append(float(line))
    myfile.close()

    return coord

def load_grid(filename):
    data = np.loadtxt(filename)
    # Check if data contains NaN values (indicating empty lines)
    if np.isnan(data).any():
        # Remove NaN values (empty lines)
        data = data[~np.isnan(data).any(axis=1)]
    lat = data[:, 0]
    lon = data[:, 1]
    thickness = data[:, 2]
    return lat, lon, thickness

def read_netcdf(file_path):
    # Open the NetCDF file
    nc = Dataset(file_path, 'r')

    # Read the variables from the file
    lats = nc.variables['x'][:]
    lons = nc.variables['y'][:]
    data = nc.variables['z'][:]

    # Close the NetCDF file
    nc.close()

    return lats,lons,data


# Function to save dictionary to a file
def save_dict_to_file(data_dict, file_path):
    with open(file_path, 'wb') as f:
        pickle.dump(data_dict, f)

# Function to load dictionary from a file
def load_dict_from_file(file_path):
    with open(file_path, 'rb') as f:
        return pickle.load(f)
