# WP6 (Tsunami) implementation status

| DTC     | Name  | Number of WFs. | Number of Steps |                                                                             
|:-------:|-------|:--------------:|:---------------:|
| DTC-T1  | Probabilistic Tsunami Forecasting (PTF) | 1 | 11 |


The current implementation status of the DTCs is indicated by the following symbols:

| Symbol     | Status            |                                                                              
|:----------:|:-----------------:|
| &#x2705;   | Fully implemented |
| &#x1F51C;  | Under development |
| &#x274C;   | Not implemented   |


## DTC-T1

* Last updated: 04/12/2024
* Updated by: Manuela Volpe

| Step     | Name                    | Status      | Registry |                                                         
|:--------:|-------------------------|:-----------:|:--------:|
| ST610101 | Scenario Player | &#x274C; |  |
| ST610102 | Listener EQ messages | &#x2705;  | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC61/WF6101) |
| ST610103 | Listener SL messages | &#x1F51C; |  |
| ST610104 | Listener GNSS messages | &#x274C; |  |
| ST610105 | Ensemble Manager | &#x2705; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC61/WF6101) |
| ST610106 | Scenario Modeling | &#x1F51C; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC61/WF6101) |
| ST610107 | Misfit Evaluator | &#x1F51C; |  |
| ST610108 | Hazard Aggregator | &#x2705;  | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC61/WF6101) |
| ST610109 | AL and Visualization | &#x2705; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC61/WF6101) |
| ST610110 | Landslide scenario Manager| &#x1F51C; |  |
| ST610111 | Earthquake modeling | &#x2705; |  |

