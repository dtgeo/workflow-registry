# WP5 (volcanoes) implementation status

| DTC     | Name  | Number of WFs. | Number of Steps |                                                                             
|:-------:|-------|:--------------:|:---------------:|
| DTC-51  | Volcanic unrest dynamics | 1 | 9 |
| DTC-52  | Volcanic ash dispersal   | 1 | 17 |
| DTC-53  | Lava flows               | 1 | 5 |
| DTC-54  | Volcanic gas dispersal   | 1 | 5 |

The current implementation status of the DTCs is indicated by the following symbols:

| Symbol     | Status            |                                                                              
|:----------:|:-----------------:|
| &#x2705;   | Fully implemented |
| &#x1F51C;  | Under development |
| &#x274C;   | Not implemented   |


## DTC-51

* Last updated: 06/11/2024
* Updated by: Chiara Montagna

| Step     | Name                    | Status      | Registry |                                                         
|:--------:|-------------------------|:-----------:|:--------:|
| ST520101 | Data Access Layer | &#x1F51C; |  |
| ST520102 | Geodetic Data Analysis & Computation | &#x1F51C; |  |
| ST520103 | GALES | &#x2705; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/WF5/DTC1/DTC51/WF5101/ST510103) |
|         | 3.1 GALES meshing and pre-processing (ST520104) |  | NA |
|         | 3.2 GALES run (ST520105) |  | NA |
|         | 3.3 GALES output produces (ST520106) |  | NA |
| ST520107 | AI Model Training | &#x2705; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/WF5/DTC1/DTC51/WF5101/ST510107) |
| ST520108 | AI Scenario Recognition (GAN) | &#x274C; |  |
| ST520109 | AI Inversion | &#x2705; | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/WF5/DTC1/DTC51/WF5101/ST510109) |
| ST520110 | AI Change Detection Training | &#x1F51C; |  |
| ST520111 | Unrest Detection | &#x1F51C; |  |
| ST520112 | Front-end | &#x2705; |  |

## DTC-52

* Last updated: 27/08/2024
* Updated by: A. Folch  

| Step     | Name                    | Status      | Registry |                                                         
|:--------:|-------------------------|:-----------:|:--------:|
| ST520101 | Data Access Layer (DAL) | &#x2705;    | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC52/WF5201/ST520101)                                                                                     
| ST520102 | MET-listener            | &#x2705;    | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC52/WF5201/ST520102)                                                                                       
| ST520103 | MET-Forecaster_mode     | &#x1F51C;   |                                                                                      
| ST520104 | MET-get-GFS             | &#x2705;    |  [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC52/WF5201/ST520104)                                                                                 
| ST520105 | MET-get-IFS             | &#x1F51C;   |                                                                                
| ST520106 | MET-get-ERA5            | &#x2705;    | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC52/WF5201/ST520106)                                                                                        
| ST520107 | ESP-listener            | &#x1F51C;   |                                                                                          
| ST520108 | ESP-Forecaster_mode     | &#x1F51C;   |                                                                                      
| ST520109 | ESP-VONA                | &#x274C;    |                                                                                       
| ST520110 | ESP-SVO                 | &#x274C;    |                                                                                          
| ST520111 | ESP-SACS                | &#x274C;    |                                                                                          
| ST520112 | Trigger                 | &#x1F51C;   |                                                                                         
| ST520113 | FALL3D                  | &#x2705;    | [repository](https://gitlab.com/dtgeo/workflow-management-system/workflow-registry/-/tree/main/DTC52/WF5201/ST520113)
|  | PostP  (ST520114)                 |     |  Not in DoW |                                                                                   
|  | Post-Maps (ST520115)               |     | Not in DoW |                                                                                        
|  | Post-SWIM  (ST520116)             |     | Not in DoW |                                                                                       
|  | Frontend (ST520117)               |     | Not in DoW |                                                                                

## DTC-53

* Last updated: 24/10/2024
* Updated by: Louise Cordrie

| Step     | Name                        | Status      | Registry |                                                         
|:--------:|-----------------------------|:-----------:|:--------:|
| ST530101 | Preparation input           | &#x1F51C;    | [repository](https://gitlab.com/louise.cordrie/DT-LAVA-WF/-/tree/main/WF5301/ST530101) |
| ST530102 | Ensemble creation           | &#x1F51C;    | [repository](https://gitlab.com/louise.cordrie/DT-LAVA-WF/-/tree/main/WF5301/ST530102) |
| ST530103 | Model run                   | &#x1F51C;    | [repository](https://gitlab.com/louise.cordrie/DT-LAVA-WF/-/tree/main/WF5301/ST530103) |
| ST530104 | Data update and assimilation| &#x1F51C;    | [repository](https://gitlab.com/louise.cordrie/DT-LAVA-WF/-/tree/main/WF5301/ST530104) |
| ST530105 | Post-Processing             | &#x1F51C;    | [repository](https://gitlab.com/louise.cordrie/DT-LAVA-WF/-/tree/main/WF5301/ST530105) |

## DTC-54

* Last updated: 11/10/2024
* Updated by: Talfan

| Step     | Name                    | Status      | Registry |                                                         
|:--------:|-------------------------|:-----------:|:--------:|
| ST540101 | Manual data             | &#x1F51C;   | not applicable |
| ST540201 | Manual data             | &#x1F51C;   | not applicable |
| ST540202 | Watch and fetch         | &#x1F51C;   | [repository](https://github.com/profskipulag/ST540102) |
| ST540203 | Emulate                 | &#x1F51C;   | [repository](https://github.com/profskipulag/ST540103) |
| ST540204 | Infer                   | &#x1F51C;   | [repository](https://github.com/profskipulag/ST540104) |
| ST540205 | Visualise               | &#x1F51C;   | [repository](https://github.com/profskipulag/ST540105) |
