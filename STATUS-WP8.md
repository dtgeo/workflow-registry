# WP8 (Anthropogenic) implementation status

| DTC     | Name  | Number of WFs. | Number of Steps |                                                                             
|:-------:|-------|:--------------:|:---------------:|
| DTC-A1  | Anthropogenic geophysical extreme forecasting (AGEF) | 4 | 6 |


The current implementation status of the DTCs is indicated by the following symbols:

| Symbol     | Status            |                                                                              
|:----------:|:-----------------:|
| &#x2705;   | Fully implemented |
| &#x1F51C;  | Under development |
| &#x274C;   | Not implemented   |


## DTC-A1

* Last updated: DD/MM/YYYY
* Updated by: 

| Step     | Name                    | Status      | Registry |                                                         
|:--------:|-------------------------|:-----------:|:--------:|
| CB-AGEF1 | Data mining and computation of attributes of georeservoir responses |  |  |
| CB-AGEF2 | Probabilistic features using various episodes |  |  |
| CB-AGEF3 | Probabilistic models (including AI models) |  |  |
| CB-AGEF4 | Deterministic geomechanical models |  |  |
| CB-AGEF5 | Demonstrator site testing |  |  |
| CB-AGEF6 | User outcomes (forecasts) |  |  |
