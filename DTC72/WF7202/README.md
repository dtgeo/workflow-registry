## DTC-E2 WF2

|   Step   | Name                           |                                                   Description                                                   |
| :------: | ------------------------------ | :-------------------------------------------------------------------------------------------------------------: |
| ST720201 | Seismicity catalog             |                                                    &#x274C;                                                     |
| ST720202 | RAMSIS                         | Project renamed to HERMES, workflow orchestration software, includes ST720202, ST720203, ST720209 and ST720210. |
| ST720203 | Activity monitoring/scheduling |                                                    &#x2934;                                                     |
| ST720204 | CRS forecast                   |        Seismicity forecast model, combined in ST720204, includes ST720205, ST720206, ST720207, ST720208         |
| ST720205 | ETAS forecast                  |                                                    &#x2934;                                                     |
| ST720206 | ETAS parameter inversion       |                                                    &#x2934;                                                     |
| ST720207 | Sequence specific model update |                                                    &#x2934;                                                     |
| ST720208 | Catalog simulation             |                                                    &#x2934;                                                     |
| ST720209 | Database                       |                                                    &#x2934;                                                     |
| ST720210 | Web service                    |                                                    &#x2934;                                                     |
| ST720211 | Forecast visualization         |                                                    &#x1F51C;                                                    |
| ST720212 | Testing                        |                                                    &#x274C;                                                     |